
#include "systems/systemregister.h"
#include "systems/systemregistrar.h"

namespace systems {

SystemRegister::SystemRegister(SystemRegistrar& parent_registrar, std::string register_name)
    : parent_registrar_(parent_registrar),
      register_name_(register_name)
{}

std::weak_ptr<AbstractSystem> SystemRegister::GetSystem(std::string system_label_id) {
    if (core_systems_.find(system_label_id) != core_systems_.end()) {
        return core_systems_.at(system_label_id);
    } else {
        return std::weak_ptr<AbstractSystem>();
    }
}

void SystemRegister::RegisterAllSystems() {
    for (auto& system : core_systems_) {
        parent_registrar_.RegisterSystem(system.first, system.second);
    }
}


}