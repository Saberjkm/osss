#ifndef VVV_ENTITIES_H_INCLUDED
#define VVV_ENTITIES_H_INCLUDED

#include <map>
#include <set>
#include <ranges>
#include <algorithm>
#include <type_traits>
#include <string>

#include "application/application.h"
namespace entities {

// What permissions the entity has for its component
// r = Read only
// kRW = Read/Write
// na = No Access (Used for error or very specific cases)
enum ComponentAccess {kRW = 0, kR, kNA};

template<typename T>
concept ComponentPair = requires(T list)
{
    { *list.begin() } -> std::convertible_to<std::pair<std::string, std::size_t>>;
};

template<typename T>
concept ComponentPairList = std::ranges::range<T> && ComponentPair<T>;

const std::string kVVVCoreEntityRegisterName = "VVVEntityRegister";

struct EntityStorageData {
    std::vector<application::ComponentRepresentation> components;
    std::size_t attribute_mask;
};

}
#endif