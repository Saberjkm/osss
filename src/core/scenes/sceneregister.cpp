#include "scenes/scenes.h"
#include "scenes/sceneregistrar.h"
#include "scenes/sceneregister.h"
#include "logic/logicregistrar.h"
#include "components/componentregistrar.h"
#include "application/application.h"
#include "application/console.h"
namespace scenes {

SceneRegister::SceneRegister(SceneRegistrar& parent_registrar, std::string register_name) 
    : parent_registrar_(parent_registrar), 
        register_name_(register_name)
{}

std::vector<SceneInfo> SceneRegister::GetSceneInfo() {
    std::vector<SceneInfo> return_list;
    for (auto& scene_data : scene_load_data_) {
        SceneInfo new_scene_info = {};
        new_scene_info.related_register = register_name_;
        new_scene_info.scene_label = scene_data.first;
        new_scene_info.override = scene_override_flags_[scene_data.first];
        return_list.emplace_back(std::move(new_scene_info));
    }
    return return_list;
}

SceneData SceneRegister::LoadScene(std::string scene_label) {
    if (!scene_load_data_.contains(scene_label)) {
        parent_registrar_.global_context().main_console().IssueWarning(
            "scenes::SceneRegister::LoadScene(std::string)", 
            "Bad Load",
            fmt::format("Trying to load Scene with ID {}, but there was no scene with that ID registered.", scene_label)
        );
        return {};
    }
    SceneData return_scene_data;
    if (!LoadScene(scene_load_data_[scene_label], return_scene_data)) {
        parent_registrar_.global_context().main_console().IssueWarning(
            "scenes::SceneRegister::LoadScene(std::string)", 
            "Bad Scene Load",
            fmt::format("Trying to load Scene with ID {} but there were some errors in loading.", scene_label)
        );
        return {};
    }
    if (!LoadSceneImpl(scene_load_data_[scene_label], return_scene_data)) {
        parent_registrar_.global_context().main_console().IssueWarning(
            "scenes::SceneRegister::LoadScene(std::string)", 
            "Bad Custom Scene Load",
            fmt::format("Trying to load Scene with ID {} but there were some errors in loading.", scene_label)
        );
        UndoLoadScene(scene_load_data_[scene_label], return_scene_data);
        return {};
    }
    return_scene_data.is_valid = true;
    return return_scene_data;
}

bool SceneRegister::UnloadScene(std::string scene_label, SceneData& scene_data) {
    if (!scene_load_data_.contains(scene_label)) {
        return false;
    }
    return true;
}

bool SceneRegister::LoadScene(const SceneLoadData& scene_load_data, SceneData& scene_data) {
    if (!LoadComponentTypes(scene_load_data, scene_data)) {
        parent_registrar_.global_context().main_console().IssueWarning(
            "scenes::SceneRegister::LoadScene(const SceneLoadData&, SceneData&)", 
            "Bad Component Type Load",
            fmt::format("Trying to load Scene with ID {} but there were some errors in loading the Component Types.", scene_load_data.scene_label)
        );
        return false;
    }
    if (!LoadSystems(scene_load_data, scene_data)) {
        parent_registrar_.global_context().main_console().IssueWarning(
            "scenes::SceneRegister::LoadScene(const SceneLoadData&, SceneData&)", 
            "Bad System Load",
            fmt::format("Trying to load Scene with ID {} but there were some errors in loading the Systems.", scene_load_data.scene_label)
        );
        UndoLoadComponentTypes(scene_data.scene_component_types);
        return false;        
    }
    if (!LoadEntities(scene_load_data, scene_data)) {
        parent_registrar_.global_context().main_console().IssueWarning(
            "scenes::SceneRegister::LoadScene(const SceneLoadData&, SceneData&)", 
            "Bad Entity Load",
            fmt::format("Trying to load Scene with ID {} but there were some errors in loading the Entities.", scene_load_data.scene_label)
        );
        UndoLoadComponentTypes(scene_data.scene_component_types);
        UndoLoadSystems(scene_data.scene_systems);
        return false;
    }
    return true;
}

bool SceneRegister::LoadSceneImpl(const SceneLoadData& scene_load_data, SceneData& scene_data) {
    return true;
}

void SceneRegister::UndoLoadScene(const SceneLoadData& scene_load_data, SceneData& scene_data) {
    bool unloaded_correctly = true;
    if (!UndoLoadComponentTypes(scene_data.scene_component_types)) unloaded_correctly = false;
    if (!UndoLoadSystems(scene_data.scene_systems)) unloaded_correctly = false;
    if (!UndoLoadEntities(scene_data.scene_entities)) unloaded_correctly = false;
    if (!unloaded_correctly) {
        parent_registrar_.global_context().main_console().IssueWarning(
            "scenes::SceneRegister::LoadComponentTypes(const SceneLoadData&, SceneData&)", 
            "Error Undoing Load of Scene",
            fmt::format("Trying to undo the loading of Scene {}", scene_load_data.scene_label)
        );      
    }
    scene_data.scene_component_types.clear();
    scene_data.scene_systems.clear();
    scene_data.scene_entities.clear();
    scene_data.is_valid = false;
}

bool SceneRegister::LoadComponentTypes(const SceneLoadData& scene_load_data, SceneData& scene_data) {
    std::vector<application::ComponentTypeData> new_component_types;  
    // Gather all the component types that have been mentioned in the scene
    std::map<std::string, application::ComponentTypeRepresentation> loaded_component_types;
    if (scene_load_data.entities.size()) {
        for (auto& entity : scene_load_data.entities) {
            for (auto& component_representation : parent_registrar_.logic_registrar().entity_registrar().GetComponentTypesFromRegister(entity.register_label, entity.scene_label, entity.entity_label)) {
                loaded_component_types.emplace(component_representation.component_label, component_representation);
            }
        }
    }
    // Makes sure that there are only unique component types and that the ones more specified take precedence 
    for (auto& component_type : scene_load_data.component_types) {
        loaded_component_types.insert_or_assign(component_type.component_label, component_type);
    }

    // Load all the component types that have been gathered
    bool error_ocurred = false;
    application::ComponentTypeData new_data;
    for (auto& component_type : loaded_component_types) {
        if (!(new_data = parent_registrar_.logic_registrar().component_registrar().LoadComponentType(component_type.second)).is_valid) {
            parent_registrar_.global_context().main_console().IssueWarning(
                "scenes::SceneRegister::LoadComponentTypes(const SceneLoadData&, SceneData&)", 
                "Component Type Load Error",
                fmt::format("Trying to load Component Type {} in Scene {} but there were some errors in loading.", component_type.first, scene_load_data.scene_label)
            );
            error_ocurred = true;
            break;
        }
        new_component_types.emplace_back(std::move(new_data));
    }
    if (error_ocurred) {
        UndoLoadComponentTypes(new_component_types);
        return false;
    }
    scene_data.scene_component_types.insert(scene_data.scene_component_types.end(), std::make_move_iterator(new_component_types.begin()), std::make_move_iterator(new_component_types.end()));
    return true;
}

bool SceneRegister::UndoLoadComponentTypes(std::vector<application::ComponentTypeData>& component_types) {
    for (auto& component_type : component_types) {
        if (!parent_registrar_.logic_registrar().component_registrar().UndoLoadComponentType(component_type).second) {
            parent_registrar_.global_context().main_console().IssueWarning(
                "scenes::SceneRegister::UndoLoadComponentTypes(std::vector<application::ComponentTypeData>&)", 
                "Component Type Undo Load Error",
                fmt::format("Trying to undo the load of Component Type {} but there were some errors in undoing the load.", component_type.label)
            );
            return false;
        }
        component_type = {};
    }
    return true;
}

bool SceneRegister::LoadSystems(const SceneLoadData& scene_load_data, SceneData& scene_data) {
    std::vector<application::SystemData> new_systems;
    // Load the systems given
    application::SystemData new_data;
    bool error_ocurred = false;
    for (auto& system : scene_load_data.systems) {
        // Load the system
        if (!((new_data = parent_registrar_.logic_registrar().system_registrar().LoadSystem(system)).is_valid)) {
            parent_registrar_.global_context().main_console().IssueWarning(
                "scenes::SceneRegister::LoadSystems(const SceneLoadData&, SceneData&)", 
                "System Load Error",
                fmt::format("Trying to load System {} in Scene {} but it couldn't be completed.", system.system_label, scene_load_data.scene_label)
            );
            error_ocurred = true;
            break;
        }
        new_systems.emplace_back(std::move(new_data));
    }
    if (error_ocurred) {
        UndoLoadSystems(new_systems);
        return false;
    }
    scene_data.scene_systems.insert(scene_data.scene_systems.end(), std::make_move_iterator(new_systems.begin()), std::make_move_iterator(new_systems.end()));
    return true;
}

bool SceneRegister::UndoLoadSystems(std::vector<application::SystemData>& systems) {
    for (auto& system : systems) {
        if (!parent_registrar_.logic_registrar().system_registrar().UndoLoadSystem(system)) {
            parent_registrar_.global_context().main_console().IssueWarning(
                "scenes::SceneRegister::UndoLoadSystems(std::vector<application::SystemData>&)", 
                "System Undo Load Error",
                fmt::format("Trying to undo the load of System {} but it couldn't be completed.", system.label)
            );
            return false;
        }
        system = {};
    }
    return true;
}

bool SceneRegister::LoadEntities(const SceneLoadData& scene_load_data, SceneData& scene_data) {
    std::vector<application::EntityData> new_entities;
    bool error_ocurred = false;
    application::EntityData new_data;
    for (auto& entity : scene_load_data.entities) { 
        if (!(new_data = parent_registrar_.logic_registrar().entity_registrar().CreateEntity(entity)).is_valid) {
            parent_registrar_.global_context().main_console().IssueWarning(
                "scenes::SceneRegister::LoadEntities(const SceneLoadData&, SceneData&)", 
                "Entity Creation Error",
                fmt::format("Trying to create Entity {}, for Scene {}, in the Register {} but couldn't create it", entity.entity_label, entity.scene_label, entity.register_label)
            );
            error_ocurred = true;
            break;
        }
        scene_data.scene_entities.emplace_back(std::move(new_data));
    }
    if (error_ocurred) {
        UndoLoadEntities(new_entities);
        return false;
    }
    return true;
}

bool SceneRegister::UndoLoadEntities(std::vector<application::EntityData>& entities) {
    for (auto& entity : entities) {
        if (!parent_registrar_.logic_registrar().entity_registrar().UndoCreation(entity)) {
            parent_registrar_.global_context().main_console().IssueWarning(
                "scenes::SceneRegister::UndoLoadEntities(std::vector<application::EntityData>&)", 
                "Entity Creation Error",
                fmt::format("Trying to undo creation of Entity {}.", entity.entity_label)
            );
            return false;
        }
        entity = {};
    }
    return true;
}


}