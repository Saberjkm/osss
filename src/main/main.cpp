
#include "application/gameapplication.h"
#include "application/settings.h"

int main() {
    application::GameApplication app;

    application::EntityRegistrarSettings entity_registrar_settings;
    application::ComponentRegistrarSettings component_registrar_settings;
    application::SystemRegistrarSettings system_registrar_settings;
    application::InputRegistrarSettings input_registrar_settings;
    application::SceneRegistrarSettings scene_registrar_settings;

    application::LogicRegistrarSettings logic_registrar_settings = { entity_registrar_settings,
                                                                     component_registrar_settings,
                                                                     system_registrar_settings,
                                                                     input_registrar_settings,
                                                                     scene_registrar_settings };
    try {
        app.Run(logic_registrar_settings);
    } catch (const std::exception& e) {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}