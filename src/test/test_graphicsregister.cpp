#include "gtest/gtest.h"

#include "test/mock_console.h"
#include "test/mock_application.h"

#include "graphics/graphicsregister.h"

using ::testing::_;

class GraphicsRegisterTest : public ::testing::Test {
    protected: 

    console::MockSystemConsole mock_console_;
    application::MockApplication mock_global_context_;

    graphics::GraphicsRegister grt1_;

    GraphicsRegisterTest() 
        : mock_global_context_(mock_console_), 
          grt1_(mock_global_context_) 
    {}
};

TEST_F(GraphicsRegisterTest, registerGraphicWorks) {
    ASSERT_TRUE(grt1_.RegisterGraphic("test1"));
    EXPECT_FALSE(grt1_.RegisterGraphic("test1"));
    ASSERT_TRUE(grt1_.HasGraphic("test1"));
    EXPECT_FALSE(grt1_.HasGraphic("test2"));
}

TEST_F(GraphicsRegisterTest, addVertexToGraphicWorks) {
    EXPECT_CALL(mock_console_, IssueWarning)
        .Times(1);

    ASSERT_TRUE(grt1_.RegisterGraphic("test1"));
    graphics::Vertex test_vertex;
    graphics::Vertex test_vertex_2;
    test_vertex_2.pos = {0.1, 0.1, 0.1};
    
    // Adding to bad Graphic
    EXPECT_FALSE(grt1_.AddVertexToGraphic("badTest", test_vertex));

    // Test with Index
    ASSERT_TRUE(grt1_.AddVertexToGraphic("test1", test_vertex));
    ASSERT_TRUE(grt1_.WillHaveVertex("test1", test_vertex));
    ASSERT_TRUE(grt1_.WillHaveIndex("test1", 0));

    // Test without Index
    ASSERT_TRUE(grt1_.AddVertexToGraphic("test1", test_vertex_2, false));
    ASSERT_TRUE(grt1_.WillHaveVertex("test1", test_vertex_2));
    EXPECT_FALSE(grt1_.WillHaveIndex("test1", 1));

}

TEST_F(GraphicsRegisterTest, addIndexToGraphicWorks) {
    grt1_.RegisterGraphic("test1");
    graphics::Vertex test_vertex;
    grt1_.AddVertexToGraphic("test1", {}, false);
    grt1_.AddVertexToGraphic("test1", {}, false);
    grt1_.AdvanceFrame();
    
    // Adding to bad Graphic
    EXPECT_FALSE(grt1_.AddIndexToGraphic("badTest", 0));

    // Adding with bounds check
    ASSERT_TRUE(grt1_.AddIndexToGraphic("test1", 0));
    ASSERT_TRUE(grt1_.WillHaveIndex("test1", 0));
    EXPECT_FALSE(grt1_.AddIndexToGraphic("test1", 10));
    EXPECT_FALSE(grt1_.WillHaveIndex("test1", 10));

}

TEST_F(GraphicsRegisterTest, advanceFrameWorks) {
}
