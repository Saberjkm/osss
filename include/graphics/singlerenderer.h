#ifndef VVV_SINGLERENDERER_H_INCLUDED
#define VVV_SINGLERENDERER_H_INCLUDED

#include "graphics/renderer.h"

namespace graphics {

class SingleRenderer : public Renderer {
    private:

    public:

    std::pair<const VkCommandBuffer*, uint32_t> command_buffers(std::size_t pipeline_wanted, std::size_t frame_wanted);

    SingleRenderer() = delete;
    SingleRenderer(GraphicsRegistrar& parent_registrar) : Renderer(parent_registrar) {}
    
    void Initialise(std::vector<std::string> pipes_wanted, std::size_t max_frames_in_flight); // Override

    bool Render(RenderInfo render_info);
};

}
#endif