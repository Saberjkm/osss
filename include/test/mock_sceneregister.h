#ifndef VVV_MOCK_SCENEREGISTER_H_INCLUDED
#define VVV_MOCK_SCENEREGISTER_H_INCLUDED

#include "gmock/gmock.h"

#include "scenes/sceneregister.h"
#include "scenes/scenes.h"

namespace scenes {

class MockSceneRegister : public SceneRegister {
    public:
    MockSceneRegister() = delete;
    MockSceneRegister(SceneRegistrar& parent_registrar, std::string register_name) : SceneRegister(parent_registrar, register_name) {}

    bool AddSceneLoadData(SceneLoadData load_data) {
        if (!scene_load_data_.contains(load_data.scene_label)) {
            scene_load_data_[load_data.scene_label] = load_data;
        }
        return false;
    }
    
    MOCK_METHOD(std::vector<SceneInfo>, GetSceneInfo, (), (override));

};

}

#endif