#ifndef VVV_SETTINGS_H_INCLUDED
#define VVV_SETTINGS_H_INCLUDED

#include <string>
#include <memory>
#include <map>

#include <GLFW/glfw3.h>

namespace scenes { class SceneRegister; }
namespace components { class ComponentRegister; }
namespace systems { class SystemRegister; }

namespace application {

struct EntityRegistrarSettings {};

struct ComponentRegistrarSettings {
    std::map<std::string, std::shared_ptr<components::ComponentRegister>> component_registers;
};

struct SystemRegistrarSettings {
    std::map<std::string, std::shared_ptr<systems::SystemRegister>> system_registers;
};

struct InputRegistrarSettings {
    GLFWwindow* window_pointer;
};

struct SceneRegistrarSettings {
    std::map<std::string, std::shared_ptr<scenes::SceneRegister>> scene_registers;
};

struct LogicRegistrarSettings {
    EntityRegistrarSettings& entity_registrar_settings;
    ComponentRegistrarSettings& component_registrar_settings;
    SystemRegistrarSettings& system_registrar_settings;
    InputRegistrarSettings& input_registrar_settings;
    SceneRegistrarSettings& scene_registrar_settings;
};
}
#endif