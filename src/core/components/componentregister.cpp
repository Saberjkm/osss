#include "components/componentregister.h"
#include "components/componentregistrar.h"

namespace components {

std::any ComponentRegister::GetComponentType(std::string component_label) {
    if (!component_types_.contains(component_label)) {
        parent_registrar_.global_context().main_console().IssueWarning(
            "components::ComponentRegister::GetComponentType(std::string)",
            "Invalid Component Label",
            fmt::format("Register {} doesn't contain component type with Label {}", register_name_, component_label)
        );
        return {};
    }
    return component_types_[component_label];
}

void ComponentRegister::LoadAllComponentTypes() {
    for (auto& component_type : component_types_) {
        parent_registrar_.RegisterComponentList(component_type.first, component_type.second);
    }
}

}