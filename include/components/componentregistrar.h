#ifndef VVV_COMPONENTREGISTRAR_H_INCLUDED
#define VVV_COMPONENTREGISTRAR_H_INCLUDED

#include <queue>
#include <string>
#include <memory>
#include <vector>
#include <unordered_map>
#include <stdexcept>
#include <typeinfo>
#include <typeindex>
#include <utility>
#include <any>

#include "components/componentlist.h"
#include "components/components.h"
#include "application/console.h"
#include "application/settings.h"

namespace logic { class LogicRegistrar; }
namespace components {
class ComponentRegister;
class VVVComponentRegister;

class ComponentRegistrar {
    private:
    application::BaseApplication& global_context_;
    logic::LogicRegistrar& related_logic_registrar_;

    std::map<std::string, ComponentList> component_lists_;
    std::map<std::string, std::function<std::any(const std::vector<std::pair<std::string, std::string>>&)>> component_constructors;

    std::shared_ptr<VVVComponentRegister> core_component_register_;

    std::map<std::string, std::shared_ptr<ComponentRegister>> component_registers_;

    public:

    ComponentRegistrar() = delete;
    
    ComponentRegistrar(application::BaseApplication& global_context, logic::LogicRegistrar& related_logic_registrar);

    ~ComponentRegistrar() {}

    application::BaseApplication& global_context() { return global_context_; }

    /**
     * @brief Initialises the registrar
     * 
     * @return true 
     * @return false 
     */
    virtual bool Initialise(const application::ComponentRegistrarSettings& component_registrar_settings);

    /**
     * @brief Does the things needed before the engine starts but after the engine is initialised
     * 
     * @return true 
     * @return false 
     */
    virtual bool FirstFrame();

    // Counts all the components of each list in the registrar
    virtual std::size_t CountAllComponents() const;

    // Advances the frame of all lists in the registrar
    virtual bool AdvanceFrame();

    virtual bool EndOfFrame();

    virtual bool HasComponentList(std::string component_label) const { return component_lists_.contains(component_label); }

    virtual bool HasDataConstructor(std::string component_label) const { return component_constructors.contains(component_label); }

    /**
     * @brief Checks if the component list of given label has the same type as supplied data
     * 
     * @param component_label Label of the component list you want to check
     * @param data The data type you want to check against
     * @return true The data types match
     * @return false The data types do not match or list did not exist
     */
    virtual bool HasSameType(const std::string& component_label, const std::any& data);

    virtual bool DataHasChanged(std::string component_label) {
        if (HasComponentList(component_label)) {
            return component_lists_.at(component_label).HasChanged();
        } else {
            return false;
        }
    }

    virtual bool DataWillChange(std::string component_label) {
        if (HasComponentList(component_label)) {
            return component_lists_.at(component_label).WillChange();
        } else {
            return false;
        }
    }

    bool AddRegister(std::string register_name, std::shared_ptr<ComponentRegister> register_pointer);

    /**
     * @brief Registers a component type in the Registrar from one of its stored register
     * 
     * @param component_label The name of the new component list
     * @param component_type The type of the newly made component list
     * @return true The component type will be registered or is already registered
     * @return false An error occurred
     */
    bool RegisterComponentList(std::string component_label, const std::any& component_type);

    // Returns the list associated with the given label or nullptr if no such list exists
    virtual ComponentList* GetList(std::string component_label) {
        if (HasComponentList(component_label)) {
            return &component_lists_.at(component_label);
        } else {
            global_context_.main_console().IssueWarning(
                "components::ComponentRegistrar::GetList(std::string)", 
                "Invalid Label",
                fmt::format("Trying to get list that doesn't exist with Label {}", component_label)
            );
            return nullptr;
        }
    }

    /**
     * @brief Get the required Component from the register
     * @tparam T Type of the requested component
     * @param component_label The ID of the label wanted
     * @param component_id The id of the Component wanted
     * @return T A copy of the component or default constructed component if error
     */
    template<class T>
    T GetComponent(std::string component_label, std::size_t component_id) {
        if (HasComponentList(component_label)) {
            if (component_lists_.at(component_label).IsOfType<T>()) {
                return component_lists_.at(component_label).get<T>(component_id);
            } else {
                global_context_.main_console().IssueWarning(
                    "components::ComponentRegistrar::GetComponent<T>(std::size_t, std::size_t)", 
                    "Invalid Type",
                    fmt::format("Trying to get a component that doesn't match the type with Label {} and Component ID {}", component_label, component_id)
                );
                return T();
            }
        } else {
            global_context_.main_console().IssueWarning(
                "components::ComponentRegistrar::GetComponent<T>(std::size_t, std::size_t)", 
                "Invalid Label ID",
                fmt::format("Trying to get a component that doesn't match the type with Label {} and Component ID {}", component_label, component_id)
            );
            return T();
        }
    }
    
    /**
     * @brief Creates a component with a given list label
     * @param list_label The component list you want to create the component with
     * @return std::size_t The id of the newly created component
     */
    virtual std::size_t CreateNewComponent(std::string component_label);
    
    /**
     * @brief Create a New Component object
     * @param list_label The component list you want to create the component with
     * @param component_data The data the component will hold
     * @return std::size_t The id of the newly created component
     */
    virtual std::size_t CreateNewComponent(std::string component_label, std::any component_data);

    /**
     * @brief Creates a component from data that is given externally from a list of <string, string> pairs
     * 
     * @param list_label The component list you want to create the component with
     * @param data The <string, string> list of which to construct the component from
     * @return std::size_t The ID of the new component
     */
    virtual std::size_t CreateNewComponent(std::string component_label, const std::vector<std::pair<std::string, std::string>>& data);


    /**
     * @brief Undoes the creation of a component for a given list
     * 
     * @param component_label The label of the list you want to undo from
     * @param component_id The component to undo
     * @return std::pair<bool, bool> The bools represent <The creation of a component was undone, The component will not exist next frame>
     */
    virtual std::pair<bool, bool> UndoCreateNewComponent(std::string component_label, std::size_t component_id);

    /**
     * @brief Deletes component for a givent list
     * 
     * @param component_label The label of the list you want to delete from
     * @param component_id The ID of the component you want to delete 
     * @return true The component was deleted
     * @return false No component was deleted
     */
    virtual bool DeleteComponent(std::string component_label, std::size_t component_id);

    /**
     * @brief Undoes the deletion of a component for a given list
     * 
     * @param component_label Label of the list to undo from
     * @param component_id The ID of the component to undo
     * @return std::pair<bool, bool> The bools represent <The deletion of a component was undone, The component will exist next frame>
     */
    virtual std::pair<bool, bool> UndoDeleteComponent(std::string component_label, std::size_t component_id);

    /**
     * @brief Calls a load event for a given Component Type which will handle flags as well
     * 
     * @param component_type The representation of the component type you want to load
     * @return true The component type will be loaded next frame
     * @return false The component type will not be loaded next frame
     */
    virtual application::ComponentTypeData LoadComponentType(const application::ComponentTypeRepresentation& component_type);

    /**
     * @brief Undoes the Load event for a given Component type which will handle the flags as well
     * 
     * @param component_label The name of the component type to undo
     * @return std::pair<bool, bool> The bools represent <A Load was undone, The list will not be loaded next frame>
     */
    virtual std::pair<bool, bool> UndoLoadComponentType(const application::ComponentTypeData& component_type);

    /**
     * @brief Unloads the Component Type which will handle the flags as well.
     * 
     * @param component_type The name of the component type to unload
     * @return true No errors occured
     * @return false An error occured
     */
    virtual bool UnloadComponentType(std::string component_label);

    /**
     * @brief Undoes the unload of a component type which will handle the flags as well
     * 
     * @param component_label The name of the component type to unload
     * @return std::pair<bool, bool> The bools represent <An Unload was undone, The list will be loaded next frame>
     */
    virtual std::pair<bool, bool> UndoUnloadComponentType(std::string component_label);

    /**
     * @brief Adds a lock to the component list
     * 
     * @param component_label The name of the component list
     * @param lock_name The name of the lock you want to apply
     * @return true The lock was added successfully 
     * @return false The lock was not added
     */
    virtual bool LockComponentType(std::string component_label, std::string lock_name);

    /**
     * @brief Undoes the adding of a lock
     * 
     * @param component_label The name of the component list
     * @param lock_name The name of the lock you want to undo
     * @return std::pair<bool, bool> The bools represent <The adding of a lock was undone, The lock will not exist next frame>
     */
    virtual std::pair<bool, bool> UndoLockComponentType(std::string component_label, std::string lock_name);

    /**
     * @brief Unlocks the lock on a component list
     * 
     * @param component_label The name of the component list
     * @param lock_name The name of the lock you want to unlock
     * @return true The lock was unlocked
     * @return false The lock was not unlocked
     */ 
    virtual bool UnlockComponentType(std::string component_label, std::string lock_name);

    /**
     * @brief Undoes the unlock of a lock in a component list
     * 
     * @param component_label The name of the component list
     * @param lock_name The name of the lock you want to undo
     * @return std::pair<bool, bool> The bools represent <The unlocking of a lock was undone, The lock will exist next frame>
     */ 
    virtual std::pair<bool, bool> UndoUnlockComponentType(std::string component_label, std::string lock_name);

};

}
#endif