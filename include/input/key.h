#ifndef VVV_KEY_H_INCLUDED
#define VVV_KEY_H_INCLUDED

namespace input {

struct Key {
    int key;
    int scancode;
    int action;
    int mods;
    int button;
    double x_pos;
    double y_pos;

    bool operator==(const Key& other_key) const {
        return (key == other_key.key           &&
                action == other_key.action     &&
                mods == other_key.mods         &&
                button == other_key.button);
    }

    bool operator<(const Key& other_key) const {
        return key < other_key.key && mods < other_key.mods && button < other_key.button;
    }
    
};

}
#endif