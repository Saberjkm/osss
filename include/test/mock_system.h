#ifndef VVV_MOCK_SYSTEM_H_INCLUDED
#define VVV_MOCK_SYSTEM_H_INCLUDED

#include "gmock/gmock.h"

#include "systems/system.h"

namespace systems {

    class MockSystem : public AbstractSystem {
        public:
        MockSystem() = delete;
        MockSystem(application::BaseApplication& globalC, logic::LogicRegistrar& relatedLR) : AbstractSystem(globalC, relatedLR) {} 
        MOCK_METHOD(bool, Load, (std::size_t id, bool lock));
    };
}

#endif