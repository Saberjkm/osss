#include "graphics/singlerenderer.h"
#include "graphics/graphicsregistrar.h"

#include <iostream>

namespace graphics {

//-- Single Renderer

std::pair<const VkCommandBuffer*, uint32_t> SingleRenderer::command_buffers(std::size_t pipeline_wanted, std::size_t frame_wanted) {
    return {&command_buffer_lists_[pipeline_wanted][frame_wanted], 1};
}

void SingleRenderer::Initialise(std::vector<std::string> pipes_wanted, std::size_t max_frames_in_flight) {
    pipelines_wanted_ = pipes_wanted;
    flight_frame_count_ = max_frames_in_flight;

    command_pools_.resize(1);
    command_buffer_lists_.resize(pipes_wanted.size());

    for (auto& command_buffer_list : command_buffer_lists_) {
        command_buffer_list.resize(flight_frame_count_);
    }

    //-- Create Command Pool 
        VkCommandPoolCreateInfo pool_info{};
        pool_info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
        pool_info.queueFamilyIndex = parent_registrar_.queue_family_indices().graphics_family.value();
        pool_info.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
        if (vkCreateCommandPool(parent_registrar_.device(), &pool_info, nullptr, &command_pools_[0]) != VK_SUCCESS) {
            throw std::runtime_error("Failed to create the renderer's command pool");
        }
    //--
    

    //-- Create Command Buffer
        VkCommandBufferAllocateInfo alloc_info{};
        alloc_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        alloc_info.commandPool = command_pools_[0];
        alloc_info.level = VK_COMMAND_BUFFER_LEVEL_SECONDARY;

        for (auto& commandBufferList : command_buffer_lists_) {
            alloc_info.commandBufferCount = (uint32_t) commandBufferList.size();
            if (vkAllocateCommandBuffers(parent_registrar_.device(), &alloc_info, commandBufferList.data()) != VK_SUCCESS) {
                throw std::runtime_error("Failed to allocate the renderer's command buffer!");
            }     
        }
    //--
    
}

bool SingleRenderer::Render(RenderInfo render_info){
    VkCommandBufferBeginInfo command_buffer_begin_info = {};
    command_buffer_begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	command_buffer_begin_info.flags = VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT;
	command_buffer_begin_info.pInheritanceInfo = &render_info.inheritance_info;
    
    VkBuffer vertex_buffers[] = {render_info.vertex_buffer};
    VkDeviceSize offsets[] = {0};

    if (vkBeginCommandBuffer(command_buffer_lists_[render_info.pipeline_wanted][render_info.frame_wanted], &command_buffer_begin_info) != VK_SUCCESS) {
        throw std::runtime_error("Failed to begin recording renderer command buffer!");
    }
    vkCmdBindPipeline(command_buffer_lists_[render_info.pipeline_wanted][render_info.frame_wanted], 
                      VK_PIPELINE_BIND_POINT_GRAPHICS, parent_registrar_.pipeline_data()->getPipeline(pipelines_wanted_[render_info.pipeline_wanted]));
    vkCmdBindVertexBuffers(command_buffer_lists_[render_info.pipeline_wanted][render_info.frame_wanted], 0, 1, vertex_buffers, offsets);
    vkCmdBindIndexBuffer(command_buffer_lists_[render_info.pipeline_wanted][render_info.frame_wanted], render_info.index_buffer, 0, VK_INDEX_TYPE_UINT32);
    vkCmdBindDescriptorSets(command_buffer_lists_[render_info.pipeline_wanted][render_info.frame_wanted], 
                            VK_PIPELINE_BIND_POINT_GRAPHICS,
                            parent_registrar_.pipeline_data()->GetPipelineLayout(pipelines_wanted_[render_info.pipeline_wanted]), 
                            0, 
                            1,
                            render_info.descriptor_set, 
                            0, 
                            nullptr);
    for (auto& object : render_info.objects_to_render) {
        vkCmdPushConstants(command_buffer_lists_[render_info.pipeline_wanted][render_info.frame_wanted],
                        parent_registrar_.pipeline_data()->GetPipelineLayout(pipelines_wanted_[render_info.pipeline_wanted]),
                        VK_SHADER_STAGE_VERTEX_BIT, 
                        0, 
                        sizeof(PushConstant), 
                        object.model_matrix);  
        vkCmdDrawIndexed(command_buffer_lists_[render_info.pipeline_wanted][render_info.frame_wanted], object.index_count, 1, object.index_offset, 0, 0);
    }
    
    if (vkEndCommandBuffer(command_buffer_lists_[render_info.pipeline_wanted][render_info.frame_wanted]) != VK_SUCCESS) {
        throw std::runtime_error("Failed to record renderer command buffer!");
    }

    return true;
}

//--

}