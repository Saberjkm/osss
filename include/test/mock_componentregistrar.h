#ifndef VVV_MOCK_COMPONENTREGISTRAR_H_INCLUDED
#define VVV_MOCK_COMPONENTREGISTRAR_H_INCLUDED

#include "gmock/gmock.h"

#include "components/componentregistrar.h"

namespace components { 
    class MockComponentRegistrar : public ComponentRegistrar {
        public:
        MockComponentRegistrar() = delete;
        MockComponentRegistrar(application::BaseApplication& globalC, logic::LogicRegistrar& rLR) : ComponentRegistrar(globalC, rLR) {}
        MOCK_METHOD(std::size_t, CountAllComponents, (), (const, override));
        MOCK_METHOD(bool, AdvanceFrame, (), (override));
        MOCK_METHOD(bool, DeleteComponent, (std::string component_label, std::size_t comp_id), (override));
        MOCK_METHOD(std::size_t, CreateNewComponent, (std::string component_label), (override));
        MOCK_METHOD(application::ComponentTypeData, LoadComponentType, (const application::ComponentTypeRepresentation& component_type), (override));
        MOCK_METHOD(bool, LockComponentType, (std::string component_label, std::string lock_name), (override));
    };
}
#endif