#include "entities/entityregister.h"
#include "entities/entityregistrar.h"
#include "entities/vvventityregister.h"
#include "application/application.h"
namespace entities {

EntityRegister::EntityRegister(EntityRegistrar& parent_registrar, std::string register_label)
    : parent_registrar_(parent_registrar),
      register_label_(register_label)
{}

std::shared_ptr<const EntityStorageData> EntityRegister::GetEntityData(const application::EntityRepresentation& entity) {
    auto entity_data = entity_map.find({entity.scene_label, entity.entity_label});
    if (entity_data != entity_map.end()) {
        return entity_data->second;
    } else {
        parent_registrar_.global_context().main_console().IssueWarning(
            "entities::EntityRegister::GetComponentTypes(scene_label, std::string)",
            "Invalid Entity",
            fmt::format("Couldn't find Entity in Register {} with Scene Label {}, and Entity Label {}", register_label_, entity.scene_label, entity.entity_label)
        );
    }
    return nullptr;
}

std::vector<application::ComponentTypeRepresentation> EntityRegister::GetComponentTypes(std::string scene_label, std::string entity_label) {
    std::vector<application::ComponentTypeRepresentation> return_list;
    auto entity = entity_map.find({scene_label, entity_label});
    if (entity != entity_map.end()) {
        if (entity->second) {
            for (auto& component : entity->second->components) {
                auto& component_type_ref = return_list.emplace_back();
                component_type_ref.component_label = component.component_label;
            }
        }

    } else {
        parent_registrar_.global_context().main_console().IssueWarning(
            "entities::EntityRegister::GetComponentTypes(scene_label, std::string)",
            "Invalid Entity",
            fmt::format("Couldn't find Entity in Register {} with Scene Label {}, and Entity Label {}", register_label_, scene_label, entity_label)
        );
    }
    return return_list;
}

}