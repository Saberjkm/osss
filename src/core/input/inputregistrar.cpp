#include <variant>
#include <string>
#include <iostream>
#include <vector>

#include "logic/logic.h"
#include "input/inputregistrar.h"
#include "logic/logicregistrar.h"
#include "fmt/format.h"

namespace input {

static void KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
    Key new_key = {key, scancode, action, mods};
    main_input_registrar->AddKey(new_key);
}

static void MouseButtonCallback(GLFWwindow* window, int button, int action, int mods) {
    Key new_key = {0, 0, action, mods, button};
    glfwGetCursorPos(window, &new_key.x_pos, &new_key.y_pos);
    main_input_registrar->AddKey(new_key);
}

bool InputRegistrar::EndOfFrame() {
    MapInputs();
    return true;
}

bool InputRegistrar::AdvanceFrame() {
    keys_used_this_frame_.clear();
    for (auto& deleted_context : next_deleted_input_context_ids_) {
        current_input_context_ids_.erase(deleted_context);
        for (auto& input_context_list : input_contexts_) {
            std::erase_if(input_context_list.second, [deleted_context](InputContext& input_context) { return input_context.context_label == deleted_context; });
        }
    }
    next_deleted_input_context_ids_.clear();
    for (std::size_t i = 0; i < next_input_contexts_.size(); i++) {
        input_contexts_[next_input_contexts_[i].priority_number].emplace_back(next_input_contexts_[i]);
    }
    next_input_contexts_.clear();
    current_input_context_ids_.insert(next_input_context_ids_.begin(), next_input_context_ids_.end());
    next_input_context_ids_.clear();
    return true;
}

bool InputRegistrar::Initialise(const application::InputRegistrarSettings& input_registrar_settings) {
    main_input_registrar = this;
    glfwSetKeyCallback(input_registrar_settings.window_pointer, KeyCallback);
    glfwSetMouseButtonCallback(input_registrar_settings.window_pointer, MouseButtonCallback);
    return true;
}

bool InputRegistrar::FirstFrame() {
    return true;
}

bool InputRegistrar::AddInputContext(InputContext input_context) {
    if (input_context.context_label.size() == 0) {
        global_context_.main_console().IssueWarning(
            "input::InputRegistrar::AddInputContext(InputContext)", 
            "Invalid Context ID",
            fmt::format("Trying to add Input Context with an empty ID {}", input_context.context_label)
        );
        return false;
    }
    if (current_input_context_ids_.contains(input_context.context_label)) {
        global_context_.main_console().IssueWarning(
            "input::InputRegistrar::AddInputContext(InputContext)", 
            "Invalid Input Context",
            fmt::format("Trying to add Input Context that already exists with ID {}", input_context.context_label)
        );
        return false;
    }
    if (next_input_context_ids_.contains(input_context.context_label)) {
        global_context_.main_console().IssueWarning(
            "input::InputRegistrar::AddInputContext(InputContext)", 
            "Invalid Input Context",
            fmt::format("Trying to add Input Context that has already been added with ID {}", input_context.context_label)
        );
        return false;
    }

    next_input_context_ids_.emplace(input_context.context_label);
    next_input_contexts_.emplace_back(input_context);
    return true;
}

bool InputRegistrar::UndoAddInputContext(std::string context_label) {
    if (context_label.size() == 0) {
        global_context_.main_console().IssueWarning(
            "input::InputRegistrar::UndoAddInputContext(std::string)", 
            "Invalid Context ID",
            fmt::format("Trying to undo adding of Input Context with an empty ID {}", context_label)
        );
        return false;
    }
    if (current_input_context_ids_.contains(context_label)) {
        global_context_.main_console().IssueWarning(
            "input::InputRegistrar::UndoAddInputContext(std::string)", 
            "Invalid Input Context",
            fmt::format("Trying to undo the addition of an Input Context that already exists with ID {}", context_label)
        );
        return false;
    }
    if (!next_input_context_ids_.contains(context_label)) {
        global_context_.main_console().IssueWarning(
            "input::InputRegistrar::UndoAddInputContext(std::string)", 
            "Invalid Input Context",
            fmt::format("Trying to undo the addition of an Input Context that hasnt been added with ID {}", context_label)
        );
        return false;
    }
    next_input_context_ids_.erase(context_label);
    return std::erase_if(next_input_contexts_, [context_label](InputContext& input_context) { return input_context.context_label == context_label; });
}

bool InputRegistrar::DeleteInputContext(std::string context_label) {
    if (context_label.size() == 0) {
        global_context_.main_console().IssueWarning(
            "input::InputRegistrar::DeleteInputContext(std::string)", 
            "Invalid Context ID",
            fmt::format("Trying to delete Input Context with empty ID {}", context_label)
        );
        return false;
    }
    if (!current_input_context_ids_.contains(context_label)) {
        global_context_.main_console().IssueWarning(
            "input::InputRegistrar::DeleteInputContext(std::string)", 
            "Invalid Context ID",
            fmt::format("Trying to delete Input Context that doesn't exist with ID {}", context_label)
        );
        return false;
    }
    if (next_deleted_input_context_ids_.contains(context_label)) {
        global_context_.main_console().IssueWarning(
            "input::InputRegistrar::DeleteInputContext(std::string)", 
            "Invalid Context ID",
            fmt::format("Trying to delete Input Context that has already been deleted with ID {}", context_label)
        );
        return false;
    }
    next_deleted_input_context_ids_.emplace(context_label);
    return true;
}

bool InputRegistrar::UndoDeleteInputContext(std::string context_label) {
    if (context_label.size() == 0) {
        global_context_.main_console().IssueWarning(
            "input::InputRegistrar::DeleteInputContext(std::string)", 
            "Invalid Context ID",
            fmt::format("Trying to undo deletion of Input Context with empty ID {}", context_label)
        );
        return false;
    }
    if (!next_deleted_input_context_ids_.contains(context_label)) {
        global_context_.main_console().IssueWarning(
            "input::InputRegistrar::DeleteInputContext(std::string)", 
            "Invalid Context ID",
            fmt::format("Trying to undo deletion of Input Context that wasn't deleted with ID {}", context_label)
        );
        return false;
    }
    next_deleted_input_context_ids_.erase(context_label);
    return true;
}


void InputRegistrar::MapInputs() {
    // Take all the keys entered this frame to map
    for (auto& key : keys_used_this_frame_) {
        // Check contexts in priority ordering (ascending order)
        for (auto& context_list : input_contexts_) {
            // Check if any of the contexts have the key
            for (auto& input_context : context_list.second) {
                if (input_context.inputs.find(key) != input_context.inputs.end()) {
                    related_logic_registrar_.DispatchEvent({
                        input_context.context_label,
                        logic::InputRegistrarTarget(*this),
                        input_context.owner,
                        logic::KeyInherentData(key),
                        logic::NoneGatherData()
                    });
                    return;
                }
            }
        }
    }
}

}