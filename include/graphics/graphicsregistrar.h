#ifndef VVV_GRAPHICSREGISTRAR_H_INCLUDED
#define VVV_GRAPHICSREGISTRAR_H_INCLUDED

#define GLM_FORCE_DEPTH_ZERO_TO_ONE

#include <iostream>
#include <fstream>
#include <stdexcept>
#include <algorithm>
#include <vector>
#include <cstring>
#include <cstdlib>
#include <cstdint>
#include <optional>
#include <set>
#include <memory>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <chrono>


#include "application/application.h"
#include "graphics/graphicsdata.h"
#include "graphics/graphicsfunctions.h"
#include "graphics/graphicsregister.h"
#include "graphics/renderer.h"
#include "graphics/singlerenderer.h"
#include "logic/logicregistrar.h"

namespace graphics {

void FramebufferResizeCallback(GLFWwindow* window, int width, int height);

VKAPI_ATTR VkBool32 VKAPI_CALL DebugCallBack(
    VkDebugUtilsMessageSeverityFlagBitsEXT message_severity,
    VkDebugUtilsMessageTypeFlagsEXT message_type,
    const VkDebugUtilsMessengerCallbackDataEXT* p_callback_data,
    void* p_user_data);

VkResult CreateDebugUtilsMessengerEXT(
    VkInstance instance,
    const VkDebugUtilsMessengerCreateInfoEXT* p_create_info,
    const VkAllocationCallbacks* p_allocator, 
    VkDebugUtilsMessengerEXT* p_debug_messenger);

void DestroyDebugUtilsMessengerEXT(
    VkInstance instance,
    VkDebugUtilsMessengerEXT debug_messenger, 
    const VkAllocationCallbacks* p_allocator);


struct SwapChainSupportDetails {
    VkSurfaceCapabilitiesKHR capabilities;
    std::vector<VkSurfaceFormatKHR> formats;
    std::vector<VkPresentModeKHR> present_modes;
};

class GraphicsRegistrar {
     private:

    logic::LogicRegistrar& related_logic_registrar_;

    GraphicsRegister graphics_register_;

    std::unique_ptr<Renderer> main_renderer_; 

    const uint32_t kScreenWidth = 1280;
    const uint32_t kScreenHeight = 960;

    VkInstance instance_;

    VkDebugUtilsMessengerEXT debug_messenger_; 

    VkPhysicalDevice physical_device_ = VK_NULL_HANDLE;
    VkDevice device_;
    VkQueue graphics_queue_;
    VkQueue present_queue_;
    QueueFamilyIndices queue_family_indices_;

    VkDescriptorPool descriptor_pool_;
    std::vector<VkDescriptorSet> descriptor_sets_;

    VkCommandPool command_pool_;
    VkCommandPool transient_command_pool_;

    VkSurfaceKHR surface_;
    VkSwapchainKHR swap_chain_;
    std::vector<VkImage> swap_chain_images_;
    std::vector<VkImageView> swap_chain_image_views_;
    std::vector<VkFramebuffer> swap_chain_frame_buffers_;
    VkFormat swap_chain_image_format_;
    VkExtent2D swap_chain_extent_;

    void* allocator_;

    VkBuffer vertex_buffer_;
    void* vertex_buffer_memory_;

    VkBuffer index_buffer_;
    void* index_buffer_memory_;

    std::vector<VkBuffer> uniform_buffers_;
    std::vector<void*> uniform_buffers_memory_;

    VkImage depth_image_;
    void* depth_image_memory_;
    VkImageView depth_image_view_;

    std::vector<VkCommandBuffer> command_buffers_;

    const size_t kMaxFramesInFlight = 2;
    size_t current_frame_ = 0;
    std::vector<unsigned long long> render_object_index_ = {0, 0};

    std::vector<VkSemaphore> image_avaliable_semaphores_;
    std::vector<VkSemaphore> render_finished_semaphores_;
    std::vector<VkFence> in_flight_fences;
    std::vector<VkFence> images_in_flight_;

    VkDescriptorSetLayout descriptor_set_layout_;

    PipelineStorage pipeline_data_;
    RenderPassStorage render_pass_data_;

    
    GLFWwindow* window_;

    const std::vector<const char*> validation_layers_ = {
        "VK_LAYER_KHRONOS_validation",
        "VK_LAYER_LUNARG_monitor"
    };

    const std::vector<const char*> device_extensions_ = {
        VK_KHR_SWAPCHAIN_EXTENSION_NAME,
        VK_KHR_SHADER_NON_SEMANTIC_INFO_EXTENSION_NAME,
    };

    glm::vec2 points[3] = {{0.0f, 0.0f}, {0.0f, -10}, {-10, -10}};
    float stroke_width_ = 0.1f;

    #ifdef NDEBUG
        const bool enableValidationLayers = false;
    #else
        const bool enableValidationLayers = true;
    #endif

    /**
     * @brief Updates the uniform buffer for the current image
     * @param current_image 
     */
    void UpdateUniformBuffer(uint32_t current_image);

    /**
     * @brief Specifically cleans up the swap chain resources 
     */
    void CleanupSwapChain();

    /**
     * @brief Initialises the games graphics window
     */
    void InitialiseWindow();


    /**
     * @brief Checks if the validation layers for Vulkan are supported
     * @return true All are supported
     * @return false At least one isn't supported
     */
    bool CheckValidationLayerSupport();
    
    /**
     * @brief Get the Required Extensions for GLFW and Debugging
     * @return std::vector<const char*> A list of the required extensions
     */
    std::vector<const char*> GetRequiredExtensions();

    /**
     * @brief Creates the vulkan instance for the application
     */
    void CreateInstance();

    /**
     * @brief Will pick the graphics card to use with vulkan
     */
    void PickPhysicalDevice();

    /**
     * @brief checks if the device has the required extensions supported
     * @param device 
     * @return true 
     * @return false 
     */
    bool CheckDeviceExtensionSupport(VkPhysicalDevice device);
    
    /**
     * @brief Checks if a device (GPU) is suitable to use for our application
     * @param device 
     */
    bool IsDeviceSuitable(VkPhysicalDevice device);

    /**
     * @brief Checks a device for specific swap chain support
     * @param device 
     * @return SwapChainSupportDetails 
     */
    SwapChainSupportDetails QuerySwapChainSupport(VkPhysicalDevice device);

    /**
     * @brief Choose the best surface format for use
     * @param avaliable_formats 
     * @return VkSurfaceFormatKHR 
     */
    VkSurfaceFormatKHR ChooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& avaliable_formats);

    /**
     * @brief Chooses the best presentation mode 
     * @param avaliable_present_modes 
     * @return VkPresentModeKHR 
     */
    VkPresentModeKHR ChooseSwapPresentMode(const std::vector<VkPresentModeKHR>& avaliable_present_modes);

    /**
     * @brief Check for resolution support
     * @param capabilities 
     * @return VkExtent2D 
     */
    VkExtent2D ChooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities);

    /**
     * @brief Create a Swap Chain
     */
    void CreateSwapChain();

    /**
     * @brief Recreates the swapchain
     */
    void RecreateSwapChain();

    /**
     * @brief Populate the image views
     */
    void CreateImageViews();

    /**
     * @brief Finds what queues are avaliable for a physical device
     * @param device  The device you want to check for
     * @return QueueFamilyIndices The flags of which families are allowed or no value if none are found
     */
    QueueFamilyIndices FindQueueFamilies(VkPhysicalDevice device);

    /**
     * @brief Create a Logical Device object that interacts with a Physical Device
     */
    void CreateLogicalDevice();

    /**
     * @brief Sets up frame buffer and related things
     */
    void CreateRenderPasses();

    /**
     * @brief Sets up the main render pass
     * @param render_pass_number 
     */
    void CreateMainRenderPass(uint32_t render_pass_number);

    /**
     * @brief Create a Graphics Pipeline
     */
    void CreateGraphicsPipelines();


    /**
     * @brief Create a Shader Module object
     * @param code 
     * @return VkShaderModule 
     */
    VkShaderModule CreateShaderModule(const std::vector<char>& code);
    
    /**
     * @brief Create a Descriptor Set Layout object
     */
    void CreateDescriptorSetLayout();

    /**
     * @brief Creates the framebuffers
     */
    void CreateFramebuffers();

    /**
     * @brief Create a Descriptor Pool 
     */
    void CreateDescriptorPool();

    /**
     * @brief Creates the Descriptor Sets
     */
    void CreateDescriptorSets();

    /**
     * @brief Creates a Command Pool
     */
    void CreateCommandPools();

    /**
     * @brief Copies a buffer from one to another
     * @param src_buffer The buffer to copy FROM
     * @param dst_buffer The buffer to copy TO
     * @param size Amount to copy
     */
    void CopyBuffer(VkBuffer src_buffer, VkBuffer dst_buffer, VkDeviceSize size);

    /**
     * @brief Create a Memory Allocator object for the buffers
     */
    void CreateMemoryAllocator();

    /**
     * @brief Create a Vertex Buffer object
     */
    void CreateVertexBuffer(VkBuffer& buffer_location, void*& memory_location, std::pair<const Vertex*, std::size_t> vertices);

    /**
     * @brief Create a Index Buffer object
     * @param buffer_location Location to put the buffer
     * @param memory_location Location for the mapped memory
     */
    void CreateIndexBuffer(VkBuffer& buffer_location, void*& memory_location, const std::vector<uint32_t>& indices);

    /**
     * @brief Create a Uniform Buffer object
     */
    void CreateUniformBuffer(VkBuffer& buffer_location, void*& memory_location);


    /**
     * @brief Finds supported format out of candidates given certain features
     * @param candidates 
     * @param tiling 
     * @param features 
     * @return VkFormat 
     */
    VkFormat FindSupportedFormat(const std::vector<VkFormat>& candidates, VkImageTiling tiling, VkFormatFeatureFlags features);

    /**
     * @brief Finds the format needed for dthe depth image
     * @return VkFormat 
     */
    VkFormat FindDepthFormat();

    /**
     * @brief Create a resources for the Depth Image
     */
    void CreateDepthResources();

    /**
     * @brief Populates the uniform buffers per swap chain image
     */
    void CreateUniformBuffers();

    /**
     * @brief Creates Command Buffers
     */
    void CreateCommandBuffers(); 

    /**
     * @brief Builds new draw commands for the buffers
     */
    void RecordCommandBuffer(std::size_t index);

    /**
     * @brief Creates the Semaphores for image avaliable/ rendering finished
     */
    void CreateSyncObjects();

    /**
     * @brief Populates the debug messenger info
     * @param create_info The info to populate
     */
    void PopulateDebugMessengerCreateInfo(VkDebugUtilsMessengerCreateInfoEXT& create_info);

    /**
     * @brief Sets up vulkan debug messenger
     */
    void SetupDebugMessenger();
    
    /**
     * @brief Creates a Surface that binds with the GLFW window 
     */
    void CreateSurface();

    /**
     * @brief Initialises vulkan for the 
     */
    void InitialiseVulkan();

    public:
    application::BaseApplication& related_application_;
    
    GraphicsRegistrar() = delete;
    GraphicsRegistrar(application::BaseApplication& related_app, logic::LogicRegistrar& related_logic_registrar) 
        : graphics_register_(related_app),
          related_application_(related_app),
          related_logic_registrar_(related_logic_registrar)
    {
        main_renderer_ = std::make_unique<SingleRenderer>(*this);
    }

    // -- Getters and Setters
    const VkDevice device() { return this->device_; }
    GLFWwindow* window() { return this->window_; }
    const VkSwapchainKHR swap_chain() { return this->swap_chain_; }
    const QueueFamilyIndices queue_family_indices() { return this->queue_family_indices_; }
    PipelineStorage* pipeline_data() { return &this->pipeline_data_; }
    // --
    bool framebuffer_resized_ = false;

    /**
     * @brief Initialises the registrar which entails initialising vulkan/glfw amongst other things
     */
    void Initialise();

    /**
     * @brief Does the steps required for the first frame but after an initialise
     * 
     */
    void FirstFrame();

    /**
     * @brief Cleans up the program for destruction
     */
    void CleanUp();

    /**
     * @brief Destroys the vertex and index buffer
     */
    void DestroyBuffer(VkBuffer buffer, void* buffer_memory);

    /**
     * @brief Draws a frame
     */
    void DrawFrame();

    bool EndOfFrame();

    /**
     * @brief Advances the drawing stuff to the next logic frame (not to be confused with the multiframe drawing)
     * 
     */
    bool AdvanceFrame();

    /**
     * @brief Gathers the render data for the render step
     * @return std::vector<RenderObject> The list of render objects
     */
    std::vector<RenderObject> GatherRenderObjects(unsigned long long& index_store);
};

}
#endif