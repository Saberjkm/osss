#include <algorithm>
#include <optional>

#include "application/gameapplication.h"
#include "application/application.h"
#include "application/console.h"
#include "application/xml.h"
#include "logic/logicregistrar.h"
#include "graphics/graphicsregistrar.h"
#include "application/settings.h"

namespace application {

void GameApplication::Run(const LogicRegistrarSettings& logic_registrar_settings) {
    related_graphics_registrar_.Initialise();
    logic_registrar_settings.input_registrar_settings.window_pointer = related_graphics_registrar_.window();
    InitialiseGame(logic_registrar_settings);
    FirstFrame();
    MainLoop();
    CleanUp();
}

void GameApplication::CleanUp(){
    related_graphics_registrar_.CleanUp();
    related_logic_registrar_.CleanUp();
    CleanUp();
}

void GameApplication::TerminateApplication() {
    should_exit_ = true;
}

void GameApplication::InitialiseGame(const LogicRegistrarSettings& logic_registrar_settings) {
    related_logic_registrar_.Initialise(logic_registrar_settings);
}

void GameApplication::FirstFrame() {
    related_logic_registrar_.FirstFrame();
    related_graphics_registrar_.FirstFrame();
}

void GameApplication::MainLoop() {
    double delta_logic_time_ = 0;
    double delta_draw_time_ = 0;
    while (!should_exit_) {
        if (glfwWindowShouldClose(related_graphics_registrar_.window())) { should_exit_ = true; break; }
        glfwPollEvents();

        //Handle Timing
        current_time_ = glfwGetTime();
        delta_logic_time_ += (current_time_ - previous_time_) / kTargetStepTime;
        delta_draw_time_ += (current_time_ - previous_time_) / target_frame_rate_;
        previous_time_ = current_time_;

        if (delta_draw_time_ >= 1.0) {
            related_graphics_registrar_.DrawFrame();
            related_graphics_registrar_.EndOfFrame();
            related_graphics_registrar_.AdvanceFrame();
            delta_draw_time_--;
        }

        if (delta_logic_time_ >= 1.0) {
            related_logic_registrar_.HandleLogicStep();
            related_logic_registrar_.EndOfFrame();
            related_logic_registrar_.AdvanceFrame();
            delta_logic_time_--;
        }
    }
    vkDeviceWaitIdle(related_graphics_registrar_.device());
}

}