#ifndef VVV_PARSER_H_INCLUDED
#define VVV_PARSER_H_INCLUDED

#include <string>
namespace strings::parser {
    // Grammar for Command String in (mostly) BNF
    // Due to hopefully allowing a large set from unicode, listing exact terminals for certain things would be needlessly complex (hence mostly BNF)
    //
    // <command-string>      ::= <opt-string> <base-command> <command-string> | <opt-string>
    // <base-command>        ::= "@" <specialised-command>
    // <specialised-command> ::= <entity> | <component> | <system>
    // <entity>              ::= "e" "[" <number> "]"
    // <component>           ::= "c" "[" <string> "|" <number> "]" | "c" "[" <number> "" <number> "]"
    // <system>              ::= "s" "[" <string> "]" | "s" "[" <number> "]" 
    // <number>              ::= <number-character> <number> | <number-character>
    // <string>              ::= <string-character> <string> | <string-character>
    // <opt-string>          ::= <string-character> <opt-string> | <string-character> | ""
    // <number-character>    ::= "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9"
    // <string-character>    ::= not <command-character> | "\" <command-character>
    // <command-character>   ::= "@" | "[" | "]" | "|" | "\"

    /**
     * @brief  The entryway into the parsing structure - only function which should be called externally
     */
    bool ParseString(std::string string_to_parse);


    bool ParseCommandString(unsigned& consumed_amount, const std::string& string_to_parse, std::string& processed_part);
    bool ParseBaseCommand(unsigned& consumed_amount, const std::string& string_to_parse, std::string& processed_part);
    bool ParseSpecialisedCommand(unsigned& consumed_amount, const std::string& string_to_parse, std::string& processed_part);
    bool ParseEntity(unsigned& consumed_amount, const std::string& string_to_parse, std::string& processed_part);
    bool ParseComponent(unsigned& consumed_amount, const std::string& string_to_parse, std::string& processed_part);
    bool ParseSystem(unsigned& consumed_amount, const std::string& string_to_parse, std::string& processed_part);
    bool ParseNumber(unsigned& consumed_amount, const std::string& string_to_parse, std::string& processed_part);
    bool ParseString(unsigned& consumed_amount, const std::string& string_to_parse, std::string& processed_part);
    bool ParseOptString(unsigned& consumed_amount, const std::string& string_to_parse, std::string& processed_part);
    bool ParseNumberCharacter(unsigned& consumed_amount, const std::string& string_to_parse, std::string& processed_part);
    bool ParseStringCharacter(unsigned& consumed_amount, const std::string& string_to_parse, std::string& processed_part);
    bool ParseCommandCharacter(unsigned& consumed_amount, const std::string& string_to_parse, std::string& processed_part);

    // Checks against a single character
    bool IsCharacter(char wanted_character, unsigned& consumed_amount, const std::string& string_to_parse, std::string& processed_part);
    // Checks against a string of characters (processes string_to_parse[consumed_amount] against each character in wanted_characters)
    bool IsCharacter(std::string wanted_characters, unsigned& consumed_amount, const std::string& string_to_parse, std::string& processed_part);
}
#endif