#include "strings/parser.h"

namespace strings::parser {

    const std::string kCommandString = "@|\\[]";

    bool ParseString(std::string string_to_parse) {
        unsigned consumed_amount = 0;
        std::string process_string = "";
        return ParseCommandString(consumed_amount, string_to_parse, process_string);
    }

    bool IsCharacter(char wanted_character, unsigned& consumed_amount, const std::string& string_to_parse, std::string& processed_part) {
        if (consumed_amount < string_to_parse.size() && string_to_parse[consumed_amount] == wanted_character) {
            processed_part = string_to_parse[consumed_amount];
            consumed_amount++;
            return true;
        }
        return false;
    }

    bool IsCharacter(std::string wanted_characters, unsigned& consumed_amount, const std::string& string_to_parse, std::string& processed_part) {
        if (consumed_amount < string_to_parse.size()) {
            for (auto char_wanted : wanted_characters) {
                if (string_to_parse[consumed_amount] == char_wanted) {
                    processed_part += char_wanted;
                    consumed_amount++;
                    return true;
                }
            }
        }
        // INVALID PARSE
        return false;
    }

    bool ParseCommandString(unsigned& consumed_amount, const std::string& string_to_parse, std::string& processed_part) {
        unsigned current_consumed = consumed_amount;
        std::string opt_str, base_command, cmd_str = "";
        if (ParseOptString(current_consumed, string_to_parse, opt_str)) {
            if (ParseBaseCommand(current_consumed, string_to_parse, base_command)) {
                if (!ParseCommandString(current_consumed, string_to_parse, cmd_str)) {
                    // INVALID PARSE
                    return false;
                }
            }
        } else {
            // INVALID PARSE
            return false;
        }
        // VALID PARSE
        consumed_amount = current_consumed;
        processed_part = opt_str + base_command + cmd_str;
        return true;
    }

    bool ParseBaseCommand(unsigned& consumed_amount, const std::string& string_to_parse, std::string& processed_part) {
        unsigned current_consumed = consumed_amount;
        std::string at_char, specialised_command = "";
        if (IsCharacter('@', current_consumed, string_to_parse, at_char)) {
            if (ParseSpecialisedCommand(current_consumed, string_to_parse, specialised_command)) {
                // VALID PARSE
                consumed_amount = current_consumed;
                processed_part = at_char + specialised_command;
                return true;        
            }
        }
        // INVALID PARSE
        return false;
    }

    bool ParseSpecialisedCommand(unsigned& consumed_amount, const std::string& string_to_parse, std::string& processed_part) {
        unsigned current_consumed = consumed_amount;
        std::string entity, component, system = "";
        if (!ParseEntity(current_consumed, string_to_parse, entity)) {
            if (!ParseComponent(current_consumed, string_to_parse, component)) {
                if (!ParseSystem(current_consumed, string_to_parse, system)) {
                    // INVALID PARSE
                    return false;
                }
            }
        }
        // VALID PARSE
        consumed_amount = current_consumed;
        processed_part = entity + component + system;
        return true;
    }

    bool ParseEntity(unsigned& consumed_amount, const std::string& string_to_parse, std::string& processed_part) {
        unsigned current_consumed = consumed_amount; 
        std::string e_char, left_bracket, number, right_bracket = "";
        if (IsCharacter('e', current_consumed, string_to_parse, e_char)) {
            if (IsCharacter('[', current_consumed, string_to_parse, left_bracket)) {
                if (ParseNumber(current_consumed, string_to_parse, number)) {
                    if (IsCharacter(']', current_consumed, string_to_parse, right_bracket)) {
                        // VALID PARSE
                        consumed_amount = current_consumed;
                        processed_part = e_char + left_bracket + number + right_bracket;
                        return true;
                    }
                }
            }
        }
        // INVALID PARSE
        return false;
    }

    bool ParseComponent(unsigned& consumed_amount, const std::string& string_to_parse, std::string& processed_part) {
        unsigned current_consumed = consumed_amount; 
        std::string c_char, left_bracket, string1, number1, comma, number2, right_bracket = "";
        if (IsCharacter('c', current_consumed, string_to_parse, c_char)) {
            if (IsCharacter('[', current_consumed, string_to_parse, left_bracket)) {
                if (!ParseString(current_consumed, string_to_parse, string1)) {
                    if (!ParseNumber(current_consumed, string_to_parse, number1)) {
                        // INVALID PARSE
                        return false;
                    }
                }
                if (IsCharacter('|', current_consumed, string_to_parse, comma)) {
                    if (ParseNumber(current_consumed, string_to_parse, number2)) {
                        if (IsCharacter(']', current_consumed, string_to_parse, right_bracket)) {
                            // VALID PARSE
                            consumed_amount = current_consumed; 
                            processed_part = c_char + left_bracket + string1 + number1 + comma + number2 + right_bracket;
                            return true;
                        }
                    }
                }
            }
        }
        // INVALID PARSE
        return false;
    }

    bool ParseSystem(unsigned& consumed_amount, const std::string& string_to_parse, std::string& processed_part) {
        unsigned current_consumed = consumed_amount; 
        std::string s_char, left_bracket, string1, number1, right_bracket = "";
        if (IsCharacter('s', current_consumed, string_to_parse, s_char)) {
            if (IsCharacter('[', current_consumed, string_to_parse, left_bracket)) {
                if (!ParseString(current_consumed, string_to_parse, string1)) {
                    if (!ParseNumber(current_consumed, string_to_parse, number1)) {
                        // INVALID PARSE
                        return false;
                    }
                }
                if (IsCharacter(']', current_consumed, string_to_parse, right_bracket)) {
                    // VALID PARSE
                    consumed_amount = current_consumed;
                    processed_part = s_char + left_bracket + string1 + number1 + right_bracket;
                    return true;
                }
            }
        }
        // INVALID PARSE
        return false;
    }
    
    bool ParseNumber(unsigned& consumed_amount, const std::string& string_to_parse, std::string& processed_part) {
        unsigned current_consumed = consumed_amount;
        std::string num_char, rest_num = "";
        if (ParseNumberCharacter(current_consumed, string_to_parse, num_char)) {
            ParseNumber(current_consumed, string_to_parse, rest_num);
            // VALID PARSE
            consumed_amount = current_consumed;
            processed_part = num_char + rest_num;
            return true;
        }
        // INVALID PARSE
        return false;
    }

    bool ParseString(unsigned& consumed_amount, const std::string& string_to_parse, std::string& processed_part) {
        unsigned current_consumed = consumed_amount;
        std::string str_char, rest_str = "";
        if (ParseStringCharacter(current_consumed, string_to_parse, str_char)) {
            ParseString(current_consumed, string_to_parse, rest_str);
            // VALID PARSE
            consumed_amount = current_consumed;
            processed_part = str_char + rest_str;
            return true;
        }
        // INVALID PARSE
        return false;
    }

    bool ParseOptString(unsigned& consumed_amount, const std::string& string_to_parse, std::string& processed_part) {
        unsigned current_consumed = consumed_amount; 
        std::string str_char, rest_opt_str = "";
        if (ParseStringCharacter(current_consumed, string_to_parse, str_char)) {
            ParseOptString(current_consumed, string_to_parse, rest_opt_str);
        }
        // VALID PARSE
        consumed_amount = current_consumed;
        processed_part = str_char + rest_opt_str;
        return true;
    }

    bool ParseNumberCharacter(unsigned& consumed_amount, const std::string& string_to_parse, std::string& processed_part) {
        if (consumed_amount < string_to_parse.size() && isdigit(string_to_parse[consumed_amount])) {
            // VALID PARSE
            processed_part = string_to_parse[consumed_amount];
            consumed_amount++;
            return true;
        }
        // INVALID PARSE
        return false;
    }

    bool ParseStringCharacter(unsigned& consumed_amount, const std::string& string_to_parse, std::string& processed_part) {
        unsigned current_consumed = consumed_amount; 
        std::string str_char = "";
        if (!ParseCommandCharacter(current_consumed, string_to_parse, str_char) || str_char == "\\") {
            // VALID PARSE
            if (current_consumed < string_to_parse.size()) {
                str_char += string_to_parse[current_consumed];
                processed_part = str_char;
                consumed_amount = current_consumed + 1;
                return true;
            }
        }
        // INVALID PARSE
        return false; 
    }

    bool ParseCommandCharacter(unsigned& consumed_amount, const std::string& string_to_parse, std::string& processed_part) {
        return (IsCharacter(kCommandString, consumed_amount, string_to_parse, processed_part));
    }
}