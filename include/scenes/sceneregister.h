#ifndef VVV_SCENEREGISTER_H_INCLUDED
#define VVV_SCENEREGISTER_H_INCLUDED

#include <string>
#include <vector>
#include <map>

#include "application/application.h"

namespace scenes {
class SceneRegistrar; 
struct SceneInfo;
struct SceneData;
struct SceneLoadData;

class SceneRegister {
    protected:

    SceneRegistrar& parent_registrar_;
    std::string register_name_;

    std::map<std::string, SceneLoadData> scene_load_data_;
    std::map<std::string, bool> scene_override_flags_;

    /**
     * @brief Loads the scene from the data given
     * 
     * @param scene_load_data The data that needs to be loaded into the engine
     * @param scene_data The currently loaded data for the scene you are loading
     * @return true The scene was loaded correctly
     * @return false There were errors loading correctly
     */
    bool LoadScene(const SceneLoadData& scene_load_data, SceneData& scene_data);

    /**
     * @brief Allows for custom implementation of a load scene step
     *        This will be the final function called
     * 
     * @param scene_load_data The data that needs to be loaded into the engine
     * @param scene_data The currently loaded data for the scene you are loading
     * @return true The scene was loaded correctly
     * @return false There were errors loading correctly
     */
    virtual bool LoadSceneImpl(const SceneLoadData& scene_load_data, SceneData& scene_data);

    /**
     * @brief Undoes the loading of a scene if there were an error
     * 
     * @param scene_data The data you wish to unload
     */
    void UndoLoadScene(const SceneLoadData& scene_load_data, SceneData& scene_data);

    /**
     * @brief Undoes loading a scene's custom data if there were an error
     * 
     * @param scene_data The data you wish to unload
     */
    virtual void UndoLoadSceneImpl(const SceneLoadData& scene_load_data, SceneData& scene_data) {};

    /**
     * @brief Loads the component types into the system for some given load data
     * 
     * @param scene_load_data The data you want to load from
     * @param scene_data The resultant references to the loaded data
     * @return true No error occurred
     * @return false An error occurred
     */
    bool LoadComponentTypes(const SceneLoadData& scene_load_data, SceneData& scene_data);

    /**
     * @brief Undoes the loading of given component types
     * 
     * @param component_types The component types you want to undo the loading of. Will leave default invalid component type data in place after undoing them.
     * @return true No errors ocurred
     * @return false Errors ocurred
     */
    bool UndoLoadComponentTypes(std::vector<application::ComponentTypeData>& component_types);

    /**
     * @brief Loads the systems into the system for some given load data
     * 
     * @param scene_load_data The data you want to load from
     * @param scene_data The resultant references to the loaded data
     * @return true No error occurred
     * @return false An error occurred
     */
    bool LoadSystems(const SceneLoadData& scene_load_data, SceneData& scene_data);

    /**
     * @brief Undoes the loading of given systems
     * 
     * @param systems The systems you want to undo the loading of. Will leave default invalid system data in place after undoing them.
     * @return true No errors ocurred
     * @return false Errors ocurred
     */
    bool UndoLoadSystems(std::vector<application::SystemData>& systems);

    /**
     * @brief Loads the entities into the system for some given load data
     * 
     * @param scene_load_data The data you want to load from
     * @param scene_data The resultant references to the loaded data
     * @return true No error occurred
     * @return false An error occurred
     */
    bool LoadEntities(const SceneLoadData& scene_load_data, SceneData& scene_data);

    /**
     * @brief Undoes the loading of given entities
     * 
     * @param entities The entities you want to undo the loading of. Will leave default invalid enttiy data in place after undoing them.
     * @return true No errors ocurred
     * @return false An error ocurred
     */
    bool UndoLoadEntities(std::vector<application::EntityData>& entities);

    public:

    SceneRegister() = delete;
    SceneRegister(SceneRegistrar& parent_registrar, std::string register_name);

    virtual ~SceneRegister() {}

    /**
     * @brief The scene registrar will use this method to gather the available scenes from this register.
     * 
     * @return std::vector<SceneInfo> The list of available scenes
     */
    virtual std::vector<SceneInfo> GetSceneInfo(); 

    /**
     * @brief Loads a scene into the parent registrar
     * 
     * @param scene_label The label ID of the scene you want to load
     * @return SceneData The reference to all the data thats been created
     */
    SceneData LoadScene(std::string scene_label);

    bool UnloadScene(std::string scene_label, SceneData& scene_data);
};

}
#endif 