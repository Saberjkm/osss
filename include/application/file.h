#ifndef VVV_FILE_H_INCLUDED
#define VVV_FILE_H_INCLUDED

#include <vector>
#include <fstream>
#include <iostream>
#include <string>
#include <filesystem>
#include <regex>

namespace application {

static std::vector<char> ReadFile(const std::string& file_name) {
    std::ifstream file(file_name, std::ios::ate | std::ios::binary);

    if (!file.is_open()) {
        throw std::runtime_error("Failed to open file! " + file_name);
    }

    size_t file_size = (size_t) file.tellg();
    std::vector<char> buffer(file_size);
    file.seekg(0);
    file.read(buffer.data(), file_size);
    file.close();
    return buffer;
}

/**
 * @brief Gathers files recursively from a list of directories. Can be supplied with a regex matcher which will then test against the file name (not the full path)
 *        Does not check if there are duplicate file names.
 * @param directories The directories to search
 * @param matcher The regex matcher
 * @return std::vector<std::filesystem::path> List of files that were found 
 */
static std::vector<std::filesystem::path> GatherFiles(const std::vector<std::filesystem::path>& directories, std::string matcher = "") {
    std::vector<std::filesystem::path> return_file_paths;
    for(auto& directory_path : directories) {
        for (auto& test_path : std::filesystem::recursive_directory_iterator(directory_path)) {
            if (test_path.is_regular_file()) {
                if (matcher.size() != 0) { /* There is a regex to match the file name with*/
                    if (std::regex_match(test_path.path().filename().generic_string(), std::regex(matcher))) {
                        return_file_paths.emplace_back(test_path);
                    }
                } else { /* No regex match*/
                    return_file_paths.emplace_back(test_path);
                }
            }
        }
    }
    return return_file_paths;
}

}

#endif