
#include "systems/system.h"
#include "logic/logic.h"
#include "entities/entity.h"
#include "application/console.h"
#include "input/input.h"

namespace systems {

bool AbstractSystem::HandleFlags(const application::FlagData& flag_data) {
    bool error = false;
    if (flag_data.flags & application::kLock) {
        if (!AddLock(flag_data.lock_owner_id)) error = true; 
    }
    return !error;
}

bool AbstractSystem::UndoHandleFlags(const application::FlagData& flag_data) {
    bool error = false;
    if (flag_data.flags & application::kLock) {
        if (!UndoAddLock(flag_data.lock_owner_id)) error = true;
    }
    return !error;
}

bool AbstractSystem::AdvanceFrame() {
    if (next_frame_.has_changed) current_frame_ = next_frame_;
    return AdvanceFrameImpl();
}

bool AbstractSystem::EndOfFrame() {
    return EndOfFrameImpl();
}

bool AbstractSystem::Load(const application::FlagData& flag_data) {
    bool did_load_event = false;
    if (!current_frame_.has_loaded && !next_frame_.has_loaded) {
        next_frame_.has_loaded = true;
        next_frame_.has_changed = true;
        did_load_event = true;
    }
    if (!HandleFlags(flag_data)) {
        global_context_.main_console().IssueWarning(
            "systems::AbstractSystem::Load(const application::FlagData&)",
            "Invalid Flags",
            fmt::format("Trying to load the system with Name {}, but there was an error handling flags.", system_name_id_)
        );
        if (did_load_event) {
            next_frame_.has_loaded = false;
            next_frame_.has_changed = false;
        }
    }
    return next_frame_.has_loaded;
}

bool AbstractSystem::UndoLoad(const application::FlagData& flag_data) {
    if (!UndoHandleFlags(flag_data)) {
        global_context_.main_console().IssueWarning(
            "systems::AbstractSystem::UndoLoad(const application::FlagData&)",
            "Invalid Flags",
            fmt::format("Trying to undo the load of a system with Name {}, but there was an error handling flags.", system_name_id_)
        );
        return !next_frame_.has_loaded;
    }
    if (!current_frame_.has_loaded && next_frame_.has_loaded && next_frame_.locks.size() == 0) {
        next_frame_.has_loaded = false;
        next_frame_.has_changed = true;
    }
    return !next_frame_.has_loaded;
}

bool AbstractSystem::Unload() {
    if (current_frame_.has_loaded && next_frame_.has_loaded && next_frame_.locks.size() == 0) {
        next_frame_.has_loaded = false;
        next_frame_.has_changed = true;
    }
    return !next_frame_.has_loaded;
}

bool AbstractSystem::UndoUnload() {
    if (current_frame_.has_loaded && !next_frame_.has_loaded) {
        next_frame_.has_loaded = true;
        next_frame_.has_changed = true;
    }
    return next_frame_.has_loaded;
}

bool AbstractSystem::AddLock(std::string lock_name) {
    if (lock_name.size() != 0) {
        if (!current_frame_.locks.contains(lock_name) && !next_frame_.locks.contains(lock_name) && next_frame_.has_loaded) {
            next_frame_.locks.emplace(lock_name);
            next_frame_.has_changed = true;
            return true;
        } else {
            global_context_.main_console().IssueWarning(
                "systems::AbstractSystem::AddLock(std::string)",
                "Invalid Lock",
                fmt::format("Trying add a lock with Name {}, but it already has a lock with that name.", lock_name)
            );
            return false;
        }
    } else {
        global_context_.main_console().IssueWarning(
            "systems::AbstractSystem::AddLock(std::string)",
            "Invalid Lock Name",
            "Trying to add a Lock with an empty name."
        );
        return false;
    }
}

bool AbstractSystem::UndoAddLock(std::string lock_name) {
    if (lock_name.size() != 0) {
        if (!current_frame_.locks.contains(lock_name) && next_frame_.locks.contains(lock_name)) {
            next_frame_.locks.erase(lock_name);
            next_frame_.has_changed = true;
            return true;
        } else {
            global_context_.main_console().IssueWarning(
                "components::AbstractSystem::UndoAddLock(std::string)",
                "Invalid Lock",
                fmt::format("Trying to undo adding a lock with Name {}, but it doesnt have a lock with that name.", lock_name)
            );
            return false;
        }
    } else {
        global_context_.main_console().IssueWarning(
            "systems::AbstractSystem::UndoAddLock(std::string)",
            "Invalid Lock Name",
            "Trying to undo adding a Lock with an empty name."
        );
        return false;
    }
}

bool AbstractSystem::Unlock(std::string lock_name) {
    if (lock_name.size() != 0) {
        if (current_frame_.locks.contains(lock_name)) {
            if (next_frame_.locks.contains(lock_name)) {
                next_frame_.locks.erase(lock_name);
                next_frame_.has_changed = true;
                return true;
            } else {
                global_context_.main_console().IssueWarning(
                    "components::AbstractSystem::UndoAddLock(std::string)",
                    "Invalid Lock",
                    fmt::format("Trying to ulock a lock with Name {}, but it has already been unlocked.", lock_name)
                );
                return false;
            }
        } else {
            global_context_.main_console().IssueWarning(
                "components::AbstractSystem::UndoAddLock(std::string)",
                "Invalid Lock",
                fmt::format("Trying to unlock a lock with Name {}, but it doesnt have a lock with that name.", lock_name)
            );
            return false;
        }
    } else {
        global_context_.main_console().IssueWarning(
            "components::AbstractSystem::Unlock(std::string)",
            "Invalid Lock Name",
            "Trying to unlock a Lock with an empty name."
        );
        return false;
    }
}

bool AbstractSystem::UndoUnlock(std::string lock_name) {
    if (lock_name.size() != 0) {
        if (current_frame_.locks.contains(lock_name)) {
            if (!next_frame_.locks.contains(lock_name)) {
                next_frame_.locks.emplace(lock_name);
                next_frame_.has_changed = true;
                return true;
            } else {
                global_context_.main_console().IssueWarning(
                    "components::AbstractSystem::UndoUnlock(std::string)",
                    "Invalid Lock",
                    fmt::format("Trying to undo an unlock of a lock with Name {}, but it has not been unlocked.", lock_name)
                );
                return false;
            }
        } else {
            global_context_.main_console().IssueWarning(
                "components::AbstractSystem::UndoUnlock(std::string)",
                "Invalid Lock",
                fmt::format("Trying to unlock a lock with Name {}, but it doesnt have a lock with that name.", lock_name)
            );
            return false;
        }
    } else {
        global_context_.main_console().IssueWarning(
            "components::AbstractSystem::UndoUnlock(std::string)",
            "Invalid Lock Name",
            "Trying to undo an unlock for a Lock with an empty name."
        );
        return false;
    }
}

bool AbstractSystem::RegisterID(std::size_t id) {
    if (current_frame_.system_id == -1 && next_frame_.system_id == -1) {
        next_frame_.system_id == id;
        return true;
    } 
    if (current_frame_.system_id != -1) {
        global_context_.main_console().IssueWarning(
            "systems::AbstractSystem::RegisterID(std::size_t)", 
            "Already Registered",
            fmt::format("Trying to register ID {} to a system that has already been registered", id)
        );
        return false;
    } else {
        global_context_.main_console().IssueWarning(
            "systems::AbstractSystem::RegisterID(std::size_t)", 
            "Already Registered",
            fmt::format("Trying to register ID {} to a system that will be registered next frame", id)
        );
        return false;
    }
}

bool AbstractSystem::HandleEvent(logic::Event& event) {
    if (!valid() || !loaded()) {
        // Dont handle events if the system is not valid or loaded
        event.can_repeat = false; // Make sure the event isn't re-added since the system cant handle it
        return true; 
    }
    return HandleEventImpl(event);
}

}