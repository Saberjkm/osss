#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_EXT_debug_printf : enable

layout(location = 0) in vec3 fragColor;
layout(location = 1) in vec4 fragTexCoord;
layout(location = 2) in float insideOrOutside;
layout(location = 3) in flat uint fillType;

layout(location = 0) out vec4 outColor;

void main() {
    if (fillType == 1) {
        //Cubic Bezier Fill
        if ((((fragTexCoord[0] * fragTexCoord[0] * fragTexCoord[0]) - (fragTexCoord[1] * fragTexCoord[2])) * insideOrOutside) >= 0) { discard; }
    } else if (fillType == 2) {
        // Quadratic Bezier Fill
        if (((fragTexCoord[0] * fragTexCoord[0] - fragTexCoord[1]) * insideOrOutside) > 0) { discard; }
    } else if (fillType == 3) {
        // Arc Fill
        if (((fragTexCoord[0] * fragTexCoord[0] + fragTexCoord[1] * fragTexCoord[1]) * insideOrOutside) > 1) { discard; }
    }
    outColor = vec4(fragColor, 1.0);
}