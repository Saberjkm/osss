#ifndef VVV_GRAPHICSREGISTER_H_INCLUDED
#define VVV_GRAPHICSREGISTER_H_INCLUDED

#include <vector>
#include <map>
#include <set>
#include <functional>

#include "graphics/graphicsdata.h"
#include "application/application.h"
#include "tinyxml2/tinyxml2.h"

namespace graphics {

//TODO: Overall doing a simple implementation and going to refine it later when I see how its used
class GraphicsRegister{
    protected:

    class Graphic {
        public:

        GraphicsRegister& parent_register_; 
        
        std::size_t vertex_count_ = 0;
        std::size_t vertex_offset_ = 0;

        std::size_t index_count_ = 0;
        std::size_t index_offset_ = 0;

        std::vector<Vertex> new_vertices_;
        std::vector<uint32_t> new_indices_; // Relative to the graphic itself not the main list
        bool has_changed_ = false;

        Graphic() = delete;
        Graphic(GraphicsRegister& pManager) : parent_register_(pManager) {}

        /**
         * @brief Following the convention of two frame system
         * @return true If the graphic was modified
         * @return false If the graphic wasn't modified
         */
        bool AdvanceFrame();

        /**
         * @brief Get the Indices that are modified with the offset
         * @return std::vector<std::size_t>&& Modified Indices
         */
        std::vector<std::size_t> GetModifiedIndices(std::size_t offset);

        /**
         * @brief Adds a new vertex to the list (ensures it is a new vertex)
         * @param new_vertex The vertex you want to add to the graphic
         * @return true Added successfully (makes sure its a new unique vertex)
         * @return false No change and the vertex wasn't unique to the graphic
         */
        bool AddNewVertex(Vertex new_vertex, bool with_index = true);

        /**
         * @brief Adds a new index to the list
         * @param new_index The index to add
         * @param bounds_checking Check if its within bounds of this graphics vertices
         * @return true Added successfully
         * @return false Not added
         */
        bool AddNewIndex(uint32_t new_index, bool bounds_checking = true);

        /**
         * @brief Check if the Index will be in the next frame
         * @param wanted_index The Index you are looking for
         * @return true The Index will exist
         * @return false The Index wont exist
         */
        bool WillHaveIndex(uint32_t wanted_index);

        /**
         * @brief Check if the Vertex will be in the next frame
         * @param wanted_vertex The Vertex you are looking for
         * @return true The Vertex will exist
         * @return false The Vertex wont exist
         */
        bool WillHaveVertex(const Vertex& wanted_vertex);
    };

    application::BaseApplication& related_application_; // Mainly here for global context

    bool has_changed_ = false;
    bool indices_changed_ = false;
    bool vertices_changed_ = false;
    
    std::map<std::string, Graphic> graphics_data_;
    std::set<std::string> load_order_;

    std::vector<Vertex> vertex_list_ = {{{-1.0f, 1.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}}};
    std::vector<uint32_t> index_list_ = {0};

    // Deafault values so you can draw something all the time
    const uint32_t default_index_count_ = 1;
    const uint32_t default_vertex_count_ = 1;

    std::vector<std::string> graphics_to_be_deleted_;

    /**
     * @brief Handles graphics that need to be 'unloaded' from the manager
     * @return true Graphic has been deleted successfully
     * @return false Graphic still exists in the system
     */
    bool HandleDeletion();

    public:

    GraphicsRegister() = delete;
    GraphicsRegister(application::BaseApplication& rA) : related_application_(rA) {}

    uint32_t default_index_count() { return default_index_count_; }

    /**
     * @brief Does end of frame operations
     * @return true All operations were successful
     * @return false If an error occurred
     */
    bool EndOfFrame();

    /**
     * @brief Following the convention of two frame system
     * @return true All operations were successful
     * @return false If an error occurred
     */
    bool AdvanceFrame();

    /**
     * @brief Get the vertices from the register and marks that they have been taken
     * 
     * @return std::vector<Vertex>& The list of vertices
     */
    std::vector<Vertex>& get_vertices();

    /**
     * @brief Get the indices from the register and marks that they have been taken
     * 
     * @return std::vector<uint32_t>& The list of indices
     */
    std::vector<uint32_t>& get_indices();

    /**
     * @brief Registers a new Graphic in the manager
     * @param new_graphic_name The name of said Graphic
     * @return true If a new graphic has been registered
     * @return false If a graphic with the given name already exists
     */
    bool RegisterGraphic(std::string new_graphic_name);

    /**
     * @brief Deletes a Graphic from the manager
     * @param graphic_name Graphic to be deleted
     * @return true Graphic won't exist in the next frame
     * @return false Graphic will still exist in the next frame
     */
    bool DeleteGraphic(std::string graphic_name);

    /**
     * @brief Checks if a graphic has been registered
     * @param graphic_name The graphics name to check
     * @return true The graphic exists
     * @return false The graphic doesn't exist
     */
    bool HasGraphic(std::string graphic_name) { return (graphics_data_.find(graphic_name) != graphics_data_.end()); }

    /**
     * @brief Gets the index offset of a Graphic
     * 
     * @param graphic_name The name of the graphic whose offset you want
     * @return uint32_t The offset or -1 if graphic doesn't exist
     */
    uint32_t GraphicIndexOffset(std::string graphic_name);

    /**
     * @brief Gets the count of indices of a Graphic
     * 
     * @param graphic_name The name of the graphic whose index count you want
     * @return uint32_t The count or -1 if graphic doesn't exist
     */
    uint32_t GraphicIndexCount(std::string graphic_name);
    
    /**
     * @brief Gets the Indices needed for a draw
     * @return const std::vector<uint16_t>& Indices to be loaded to the GPU
     */
    const std::vector<uint32_t>& loadable_indices() {return index_list_; }

    /**
     * @brief A check if the indices have changed thus requiring a load
     * @return true 
     * @return false 
     */
    bool needs_indices_loaded() { return indices_changed_; }

    /**
     * @brief Get the Vertices needed for a draw
     * @return const std::vector<Vertex>& Vertices to be loaded to the GPU
     */
    const std::vector<Vertex>& loadable_vertices() { return vertex_list_; } 
            
    /**
     * @brief A check if the vertices have changed thus requiring a load
     * @return true 
     * @return false 
     */
    bool needs_vertices_loaded() { return vertices_changed_; }

    /**
     * @brief Get the vertices related to a given Graphic
     * @param graphic_name The name of the wanted Graphic
     * @return std::vector<const Vertex&> List of the vertices
     */
    std::vector<std::reference_wrapper<const Vertex>> GetGraphicsVertices(std::string graphic_name);

    /**
     * @brief Get the indices related to a given Graphic
     * @param graphic_name The name of the wanted Graphic
     * @return std::vector<uint32_t> List of the indices
     */
    std::vector<uint32_t> GetGraphicsIndices(std::string graphic_name);

    /**
     * @brief Adds a new vertex to a given Graphic
     * @param graphic_name The name of the related Graphic
     * @param new_vertex The vertex to add
     * @param with_index Whether or not an associated index will be added to the Graphic 
     * @return true The vertex was addded successfully
     * @return false The vertex was not added
     */
    bool AddVertexToGraphic(std::string graphic_name, Vertex new_vertex, bool with_index = true);
    
    /**
     * @brief Adds a new index to a given Graphic
     * @param graphic_name The name of the related Graphic
     * @param new_index The index to add
     * @param bounds_checking Adds a check whether the index is within the bound of the graphc (i.e an index of 29 is not accepted within a graphic of only 20 vertices)
     * @return true The index  was added
     * @return false The index was not added
     */
    bool AddIndexToGraphic(std::string graphic_name, uint32_t new_index, bool bounds_checking = true);

    /**
     * @brief Checks if a vertex exists in the current frame for a given Graphic
     * @param graphic_name The graphic you want to check for
     * @param wanted_vertex The vertex you want to check for
     * @return true The vertex exists
     * @return false The vertex doesn't exist
     */
    bool HasVertex(std::string graphic_name, const Vertex& wanted_vertex);
    
    /**
     * @brief Checks if a vertex will exist in the next frame for a given Graphic
     * @param graphic_name The graphic you want to check for
     * @param wanted_vertex The vertex you want to check for
     * @return true The vertex will exist
     * @return false The vertex won't exist
     */
    bool WillHaveVertex(std::string graphic_name, const Vertex& wanted_vertex);

    /**
     * @brief Checks if an index exists in the current frame for a given Graphic
     * @param graphic_name The graphic you want to check for
     * @param wanted_index The index you want to check for
     * @return true The index exists
     * @return false The index doesn't exist
     */
    bool HasIndex(std::string graphic_name, uint32_t wanted_index);

    /**
     * @brief Checks if an index will exist in the next frame frame for a given Graphic
     * @param graphic_name The graphic you want to check for
     * @param wanted_index The index you want to check for
     * @return true The index will exist
     * @return false The index won't exist
     */
    bool WillHaveIndex(std::string graphic_name, uint32_t wanted_index);


    void set_has_changed(bool new_value) { has_changed_ = new_value; }
    bool has_changed() { return has_changed_; }

    std::size_t vertex_count() { return vertex_list_.size(); }
    std::size_t index_count() { return index_list_.size(); }

};

}
#endif