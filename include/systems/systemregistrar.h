#ifndef VVV_SYSTEMREGISTRAR_H_INCLUDED
#define VVV_SYSTEMREGISTRAR_H_INCLUDED

#include <vector>
#include <memory>
#include <map>

#include "application/application.h"
#include "application/console.h"
#include "application/settings.h"
#include "logic/logic.h"
#include "systems/systems.h"

namespace logic { class LogicRegistrar;}
namespace input { struct Key; }
namespace systems { 
class AbstractSystem;
class SystemRegister;
class VVVSystemRegister;

template<class D, class B>
concept Derived = std::is_base_of<B,D>::value;

class SystemRegistrar {
    protected:

    application::BaseApplication& global_context_;
    logic::LogicRegistrar& related_logic_registrar_;
    
    std::map<std::string, std::weak_ptr<AbstractSystem>> system_list_;

    std::shared_ptr<VVVSystemRegister> core_system_register_;
    std::map<std::string, std::shared_ptr<systems::SystemRegister>> system_registers_;
    
    /**
     * @brief Allows for the inheritence of the Initialise function
     * 
     * @param system_registrar_settings 
     * @return true 
     * @return false 
     */
    virtual bool InitialiseImpl(const application::SystemRegistrarSettings& system_registrar_settings) { return true; }

    public:
    virtual ~SystemRegistrar() {}

    SystemRegistrar() = delete;
    
    SystemRegistrar(application::BaseApplication& global_context, logic::LogicRegistrar& related_logic_registrar);

    application::BaseApplication& global_context() { return global_context_; }
    logic::LogicRegistrar& logic_registrar() { return related_logic_registrar_; }
    
    bool HasSystem(std::string system_label) {
        return (system_list_.contains(system_label));
    }

    bool SystemDataHasChanged(std::string system_label);
    bool SystemDataWillChange(std::string system_label);

    std::shared_ptr<systems::SystemRegister> GetSystemRegister(std::string register_name);

    /**
     * @brief Sets the register to look at when referring to core systems
     * 
     * @tparam T Derived class of a systems::SystemRegister 
     * @param system_register The register that you want to set in the registrar
     * @return true The register was registered successfully
     * @return false An error occured
     */
    template<Derived<systems::SystemRegister> T> 
    bool LoadSystemRegister(std::string label, std::shared_ptr<T> system_register) {
        if (!system_register) {
            global_context_.main_console().IssueWarning(
                "systems::AbstractSystemRegistrar::LoadSystemRegister(std::string, std::shared_ptr<T>)", 
                "Invalid Register",
                fmt::format("Trying to add register with Label {}, but register pointer is null", label)
            );
            return false;
        }
        if (system_registers_.find(label) == system_registers_.end()) {
            system_registers_[label] = system_register;
            return true;
        } else {
            global_context_.main_console().IssueWarning(
                "systems::AbstractSystemRegistrar::LoadSystemRegister(std::string, std::shared_ptr<T>)", 
                "Invalid Label",
                fmt::format("Trying to register system register with Label {}", label)
            );
            return false;
        }
    }

    /**
     * @brief Advances the registrars frame
     * 
     * @return true Successful
     * @return false An error occurred 
     */
    virtual bool AdvanceFrame();

    /**
     * @brief Ends the frame of the registrar
     * 
     * @return true 
     * @return false 
     */
    virtual bool EndOfFrame() ;

    /**
     * @brief Initialises the registrar
     * 
     * @return true 
     * @return false 
     */
    bool Initialise(const application::SystemRegistrarSettings& system_registrar_settings);
    
    /**
     * @brief Does the things needed before the engine starts but after the engine is initialised
     * 
     * @return true 
     * @return false 
     */
    bool FirstFrame();

    /**
     * @brief Counts the amount of systems registered in this registrar
     */
    virtual std::size_t CountSystems() { return this->system_list_.size(); }

    /**
     * @brief Registers a system into the registrar
     * 
     * @param register_label What register carries the definition of the system
     * @param system_label The label of the System
     * @return true The system is now registered into the system or was already
     * @return false The system was not registered
     */
    virtual bool RegisterSystem(std::string register_label, std::string system_label);

    /**
     * @brief Registers a system with the registrar
     * 
     * @param system_label Name of the system label
     * @param system_pointer Adress of the system
     * @return true The system is now registered into the system or was already
     * @return false The system was not registered
     */
    virtual bool RegisterSystem(std::string system_label, std::weak_ptr<AbstractSystem> system_pointer);

    /**
     * @brief Calls a load event for a registered system
     * 
     * @param system The system you want to load
     * @return true Load event was successful
     * @return false Load event failed or could not find system
     */
    virtual application::SystemData LoadSystem(const application::SystemRepresentation& system);

    /**
     * @brief Calls the undo load event for a registered system
     * 
     * @param system_label The label of the system
     * @return true Undo load event was successful
     * @return false Undo load event failed or could not find system
     */
    virtual bool UndoLoadSystem(const application::SystemData& system);

    /**
     * @brief Calls an unload event for a registered system
     * 
     * @param system_label The label of the system you want to unload
     * @return true Unload event was successful
     * @return false Unload event failed or could not find system
     */
    virtual bool UnloadSystem(std::string system_label);

    /**
     * @brief Calls the undo unload event for a registered system
     * 
     * @param system_label The label of the system you want to unload
     * @return true The Undo Unload event was successful
     * @return false The Undo Unload event failed or could not find system
     */
    virtual bool UndoUnloadSystem(std::string system_label);

    /**
     * @brief Adds a lock for the given system
     * 
     * @param system_label The label of the system
     * @param lock_name The name of the lock to add
     * @return true The lock was added
     * @return false No lock was added
     */
    virtual bool LockSystem(std::string system_label, std::string lock_name);

    /**
     * @brief Undoes the adding of a lock for the given system
     * 
     * @param system_label The label of the system
     * @param lock_name The name of the lock to undo
     * @return true The locks addition was undone
     * @return false No lock addition was undone
     */
    virtual bool UndoLockSystem(std::string system_label, std::string lock_name);

    /**
     * @brief Unlocks a lock on the given system
     * 
     * @param system_label The label of the system
     * @param lock_name The lock you want to unlock
     * @return true The lock was unlocked
     * @return false No lock was unlocked
     */
    virtual bool UnlockSystem(std::string system_label, std::string lock_name);

    /**
     * @brief Undoes the unlocking of a lock for a given system
     * 
     * @param system_label The label of the said system
     * @param lock_name The name of the lock you wish to undo the unlocking of
     * @return true The unlock was undone
     * @return false No undoing of an unlock was done
     */
    virtual bool UndoUnlockSystem(std::string system_label, std::string lock_name);

    /**
     * @brief Handles an event that is sent to a specific system
     * 
     * @param event The event to handle
     * @return true Event had no errors whilst handling
     * @return false Event had some errors whilst handling
     */
    bool HandleSystemEvent(logic::Event& event, std::string system_label);

    /**
     * @brief Handles an event that is specific to the system registrar
     * 
     * @param event The event to handle
     * @return true Event had no errors whilst handling
     * @return false Event had some errors whilst handling
     */
    bool HandleRegistrarEvent(logic::Event& event);
    

};

}
#endif