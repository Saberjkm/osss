#include "gtest/gtest.h"

#include "test/mock_logicregistrar.h"
#include "input/inputregistrar.h"
#include "test/mock_application.h"

class InputRegistrarTest :  public ::testing::Test {
    protected:
    console::MockSystemConsole mock_console_;
    application::MockApplication mock_global_context_;
    logic::MockLogicRegistrar mock_logic_registrar_;
    entities::MockEntityRegistrar mock_entity_registrar_;
    components::MockComponentRegistrar mock_component_registrar_;
    systems::MockSystemRegistrar mock_system_registrar_;
    input::InputRegistrar irt1_;

    InputRegistrarTest()
        : mock_global_context_(mock_console_),
          mock_logic_registrar_(mock_global_context_, mock_entity_registrar_, mock_component_registrar_, mock_system_registrar_),
          mock_entity_registrar_(mock_global_context_, mock_logic_registrar_),
          mock_component_registrar_(mock_global_context_, mock_logic_registrar_),
          mock_system_registrar_(mock_global_context_, mock_logic_registrar_),
          irt1_(mock_global_context_, mock_logic_registrar_) 
    {}
};

TEST_F(InputRegistrarTest, AddKey_Works) {
    input::Key test_key = {};
    irt1_.AddKey(test_key);
    EXPECT_EQ(irt1_.GetKeysUsedThisFrame().size(), (std::size_t) 1);
}

TEST_F(InputRegistrarTest, AddInputContext_EmptyContextID) {
    EXPECT_FALSE(irt1_.AddInputContext({}));
    EXPECT_EQ(irt1_.InputContextCount(), (std::size_t) 0);
    irt1_.EndOfFrame();
    irt1_.AdvanceFrame();
    EXPECT_FALSE(irt1_.HasContext(""));
    EXPECT_EQ(irt1_.InputContextCount(), (std::size_t) 0);
}

TEST_F(InputRegistrarTest, AddInputContext_AlreadyExists) {
    irt1_.AddInputContext({"test_1"});
    irt1_.EndOfFrame();
    irt1_.AdvanceFrame();

    EXPECT_FALSE(irt1_.AddInputContext({"test_1"}));
    irt1_.EndOfFrame();
    irt1_.AdvanceFrame();
    ASSERT_TRUE(irt1_.HasContext("test_1"));
    EXPECT_EQ(irt1_.InputContextCount(), (std::size_t) 1);
}

TEST_F(InputRegistrarTest, AddInputContext_AlreadyAdded) {
    irt1_.AddInputContext({"test_1"});
    EXPECT_FALSE(irt1_.AddInputContext({"test_1"}));
    irt1_.EndOfFrame();
    irt1_.AdvanceFrame();
    ASSERT_TRUE(irt1_.HasContext("test_1"));
    EXPECT_EQ(irt1_.InputContextCount(), (std::size_t) 1);
}

TEST_F(InputRegistrarTest, AddInputContext_Works) {
    ASSERT_TRUE(irt1_.AddInputContext({"test_1"}));
    irt1_.EndOfFrame();
    irt1_.AdvanceFrame();
    ASSERT_TRUE(irt1_.HasContext("test_1"));
    EXPECT_EQ(irt1_.InputContextCount(), (std::size_t) 1);
}

TEST_F(InputRegistrarTest, UndoAddInputContext_EmptyID) {
    irt1_.AddInputContext({"test_1"});
    EXPECT_FALSE(irt1_.UndoAddInputContext(""));
    irt1_.EndOfFrame();
    irt1_.AdvanceFrame();
    ASSERT_TRUE(irt1_.HasContext("test_1"));
    EXPECT_EQ(irt1_.InputContextCount(), (std::size_t) 1);
}

TEST_F(InputRegistrarTest, UndoAddInputContext_BadID) {
    irt1_.AddInputContext({"test_1"});
    EXPECT_FALSE(irt1_.UndoAddInputContext("test_bad"));
    irt1_.EndOfFrame();
    irt1_.AdvanceFrame();
    ASSERT_TRUE(irt1_.HasContext("test_1"));
    EXPECT_EQ(irt1_.InputContextCount(), (std::size_t) 1);
}

TEST_F(InputRegistrarTest, UndoAddInputContext_AlreadyAdded) {
    irt1_.AddInputContext({"test_1"});
    irt1_.EndOfFrame();
    irt1_.AdvanceFrame();
    EXPECT_FALSE(irt1_.UndoAddInputContext("test_bad"));
    irt1_.EndOfFrame();
    irt1_.AdvanceFrame();
    ASSERT_TRUE(irt1_.HasContext("test_1"));
    EXPECT_EQ(irt1_.InputContextCount(), (std::size_t) 1);
}

TEST_F(InputRegistrarTest, UndoAddInputContext_Works) {
    irt1_.AddInputContext({"test_1"});
    ASSERT_TRUE(irt1_.UndoAddInputContext("test_1"));
    irt1_.EndOfFrame();
    irt1_.AdvanceFrame();
    EXPECT_FALSE(irt1_.HasContext("test_1"));
    EXPECT_EQ(irt1_.InputContextCount(), (std::size_t) 0);
}

TEST_F(InputRegistrarTest, DeleteInputContext_EmptyID) {
    irt1_.AddInputContext({"test_1"});
    irt1_.EndOfFrame();
    irt1_.AdvanceFrame();

    EXPECT_FALSE(irt1_.DeleteInputContext(""));
    irt1_.EndOfFrame();
    irt1_.AdvanceFrame();
    ASSERT_TRUE(irt1_.HasContext("test_1"));
    EXPECT_EQ(irt1_.InputContextCount(), (std::size_t) 1);
}

TEST_F(InputRegistrarTest, DeleteInputContext_BadID) {
    irt1_.AddInputContext({"test_1"});
    irt1_.EndOfFrame();
    irt1_.AdvanceFrame();

    EXPECT_FALSE(irt1_.DeleteInputContext("test_bad"));
    irt1_.EndOfFrame();
    irt1_.AdvanceFrame();
    ASSERT_TRUE(irt1_.HasContext("test_1"));
    EXPECT_EQ(irt1_.InputContextCount(), (std::size_t) 1);
}

TEST_F(InputRegistrarTest, DeleteInputContext_AlreadyDeleted) {
    irt1_.AddInputContext({"test_1"});
    irt1_.EndOfFrame();
    irt1_.AdvanceFrame();

    ASSERT_TRUE(irt1_.DeleteInputContext("test_1"));
    EXPECT_FALSE(irt1_.DeleteInputContext("test_1"));
    irt1_.EndOfFrame();
    irt1_.AdvanceFrame();
    EXPECT_EQ(irt1_.InputContextCount(), (std::size_t) 0);
    EXPECT_FALSE(irt1_.HasContext("test_1"));
}

TEST_F(InputRegistrarTest, DeleteInputContext_Works) {
    irt1_.AddInputContext({"test_1"});
    irt1_.EndOfFrame();
    irt1_.AdvanceFrame();

    ASSERT_TRUE(irt1_.DeleteInputContext("test_1"));
    irt1_.EndOfFrame();
    irt1_.AdvanceFrame();
    EXPECT_EQ(irt1_.InputContextCount(), (std::size_t) 0);
    EXPECT_FALSE(irt1_.HasContext("test_1"));
}

TEST_F(InputRegistrarTest, UndoDeleteInputContext_EmptyID) {
    irt1_.AddInputContext({"test_1"});
    irt1_.EndOfFrame();
    irt1_.AdvanceFrame();

    ASSERT_TRUE(irt1_.DeleteInputContext("test_1"));
    EXPECT_FALSE(irt1_.UndoDeleteInputContext(""));
    irt1_.EndOfFrame();
    irt1_.AdvanceFrame();
    EXPECT_FALSE(irt1_.HasContext("test_1"));
    EXPECT_EQ(irt1_.InputContextCount(), (std::size_t) 0);
}

TEST_F(InputRegistrarTest, UndoDeleteInputContext_BadID) {
    irt1_.AddInputContext({"test_1"});
    irt1_.EndOfFrame();
    irt1_.AdvanceFrame();

    ASSERT_TRUE(irt1_.DeleteInputContext("test_1"));
    EXPECT_FALSE(irt1_.UndoDeleteInputContext("test_bad"));
    irt1_.EndOfFrame();
    irt1_.AdvanceFrame();
    EXPECT_FALSE(irt1_.HasContext("test_1"));
    EXPECT_EQ(irt1_.InputContextCount(), (std::size_t) 0);
}

TEST_F(InputRegistrarTest, UndoDeleteInputContext_AlreadyUndone) {
    irt1_.AddInputContext({"test_1"});
    irt1_.EndOfFrame();
    irt1_.AdvanceFrame();

    ASSERT_TRUE(irt1_.DeleteInputContext("test_1"));
    ASSERT_TRUE(irt1_.UndoDeleteInputContext("test_1"));
    EXPECT_FALSE(irt1_.UndoDeleteInputContext("test_1"));
    irt1_.EndOfFrame();
    irt1_.AdvanceFrame();
    ASSERT_TRUE(irt1_.HasContext("test_1"));
    EXPECT_EQ(irt1_.InputContextCount(), (std::size_t) 1);
}

TEST_F(InputRegistrarTest, UndoDeleteInputContext_Works) {
    irt1_.AddInputContext({"test_1"});
    irt1_.EndOfFrame();
    irt1_.AdvanceFrame();

    ASSERT_TRUE(irt1_.DeleteInputContext("test_1"));
    ASSERT_TRUE(irt1_.UndoDeleteInputContext("test_1"));
    irt1_.EndOfFrame();
    irt1_.AdvanceFrame();
    ASSERT_TRUE(irt1_.HasContext("test_1"));
    EXPECT_EQ(irt1_.InputContextCount(), (std::size_t) 1);
}