#ifndef VVV_CONSOLE_H_INCLUDED
#define VVV_CONSOLE_H_INCLUDED

#include <vector>
#include <string>

#define FMT_HEADER_ONLY
#include "fmt/format.h"

namespace console {
    // Error: If the game cant run then use this (i.e core systems breaking)
    // Warning: If the game can continue use this
    enum MessageType { kError, kWarning, kCommand, kDebug, kText };

    class SystemConsole{
        private:
        std::vector<std::pair<MessageType, std::string>> message_list_;

        public:
        virtual void ThrowError(std::string source, std::string reason, std::string description);
        virtual void IssueWarning(std::string source, std::string reason, std::string description);
    };
}

#endif