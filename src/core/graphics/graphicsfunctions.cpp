#include <set>

#include "graphics/graphicsfunctions.h"

namespace graphics {
// -- General functions
    float RoundToZero(float val) {
        if (val < kRoundError && val > -kRoundError)
            return 0;
        return val;
    }

    bool ApproxEqual(float f0, float f1)
    {
        return fabsf(f0 - f1) < kRoundError;
    }

    bool ApproxEqual(const glm::vec3& v0, const glm::vec3& v1)
    {
        return glm::pow((v0 - v1).length(), 2) < kRoundError * kRoundError;
    }
// -- 

ClassifiedCurve ClassifyCurve(glm::vec3 control_points[4]) {
    glm::vec3 b0(control_points[0].x, control_points[0].y, 1.0f);
    glm::vec3 b1(control_points[1].x, control_points[1].y, 1.0f);
    glm::vec3 b2(control_points[2].x, control_points[2].y, 1.0f);
    glm::vec3 b3(control_points[3].x, control_points[3].y, 1.0f);

    float a1 = glm::dot(b0, glm::cross(b3, b2));
    float a2 = glm::dot(b1, glm::cross(b0, b3));
    float a3 = glm::dot(b2, glm::cross(b1, b0));

    glm::vec3 ds({a1 - 2.0f * a2 + 3.0f * a3, -a2 + 3.0f * a3, 3.0f * a3});
    ds = glm::normalize(ds);
    float d1 = ds.x;
    float d2 = ds.y;
    float d3 = ds.z;

    float term0 = 3.0f * d2 * d2 - 4.0f * d1 * d3;
    float discriminant = d1 * d1 * term0;

    d1 = RoundToZero(d1);
    d2 = RoundToZero(d2);
    d3 = RoundToZero(d3);
    discriminant = RoundToZero(discriminant);

    if (ApproxEqual(b0, b1) && ApproxEqual(b0, b2) && ApproxEqual(b0, b3)) {
        return ClassifiedCurve(CubicBezierCurveType::kPoint, d1, d2, d3);
    }

    if (!discriminant) {
        if (!d1 && !d2) {
            if (!d3) { return ClassifiedCurve(CubicBezierCurveType::kLine, d1 ,d2, d3); }
            return ClassifiedCurve(CubicBezierCurveType::kQuadratic, d1, d2, d3);
        }
        if (!d1) { return ClassifiedCurve(CubicBezierCurveType::kCusp, d1, d2, d3); }
        if (term0 < 0) { return ClassifiedCurve(CubicBezierCurveType::kLoop, d1, d2, d3); }  // Cusp with inflection at infinity 
        return ClassifiedCurve(CubicBezierCurveType::kSerpentine, d1, d2, d3);
    }

    if (discriminant > 0) { return ClassifiedCurve(CubicBezierCurveType::kSerpentine, d1, d2, d3); }

    // < 0
    return ClassifiedCurve(CubicBezierCurveType::kLoop, d1, d2, d3);
}

std::vector<Vertex> GenerateCuvicBezierCurve(glm::vec3 control_points[4], glm::vec3 colour, FillSide fill_side)  {
    bool reverse_orientation = false;

    ClassifiedCurve curve = ClassifyCurve(control_points);

    glm::mat4x3 tex_coordinates; 
    switch (curve.type) {
        case CubicBezierCurveType::kSerpentine: {
            float t1 = sqrtf(9.0f * curve.d2 * curve.d2 - 12 * curve.d1 * curve.d3);
            float ls = 3.0f * curve.d2 - t1;
            float lt = 6.0f * curve.d1;
            float ms = 3.0f * curve.d2 + t1;
            float mt = lt;
            float ltMinusLs = lt - ls;
            float mtMinusMs = mt - ms;

            tex_coordinates[0] = {ls * ms, ls * ls * ls, ms * ms * ms};
            tex_coordinates[1] = {kOneThird * (3.0f * ls * ms - ls * mt - lt * ms), ls * ls * (ls - lt), ms * ms * (ms - mt)};
            tex_coordinates[2] = {kOneThird * (lt * (mt - 2.0f * ms) + ls * (3.0f * ms - 2.0f * mt)), ltMinusLs * ltMinusLs * ls, mtMinusMs * mtMinusMs * ms};
            tex_coordinates[3] = {ltMinusLs * mtMinusMs, -(ltMinusLs * ltMinusLs * ltMinusLs), -(mtMinusMs * mtMinusMs * mtMinusMs)};
            if (curve.d1 < 0.0f) {reverse_orientation = true; }
            break;
        }
        case CubicBezierCurveType::kLoop: {
            float t1 = sqrtf(4.0f * curve.d1 * curve.d3 - 3.0f * curve.d2 * curve.d2);
            float ls = curve.d2 - t1;
            float lt = 2.0f * curve.d1;
            float ms = curve.d2 + t1;
            float mt = lt;

            // Check for rendering artifacts
            float ql = ls / lt;
            float qm = ms / mt;
            if (0.0f < ql && ql < 1.0f) {
                return {};
            }

            if (0.0f < qm && qm < 1.0f) {
                return {};
            }
            
            float ltMinusLs = lt - ls;
            float mtMinusMs = mt - ms;
            tex_coordinates[0] = {ls * ms, ls * ls * ms, ls * ms * ms};
            tex_coordinates[1] = {kOneThird * (-ls * mt - lt * ms + 3.0f * ls * ms),
                                 -kOneThird * ls * (ls * (mt - 3.0f * ms) + 2.0f * lt * ms),
                                 -kOneThird * ms * (ls * (2.0f * mt - 3.0f * ms) + lt * ms)};
            tex_coordinates[2] = {kOneThird * (lt * (mt - 2.0f * ms) + ls * (3.0f * ms - 2.0f * mt)),
                                 kOneThird * (lt - ls) * (ls * (2.0f * mt - 3.0f * ms) + lt * ms),
                                 kOneThird * (mt - ms) * (ls * (mt - 3.0f * ms) + 2.0f * lt * ms)};
            tex_coordinates[3] = {ltMinusLs * mtMinusMs, -(ltMinusLs * ltMinusLs) * mtMinusMs, -ltMinusLs * mtMinusMs * mtMinusMs};
            reverse_orientation = ((curve.d1 > 0.0f &&tex_coordinates[0].x < 0.0f) || (curve.d1 < 0.0f &&tex_coordinates[0].x > 0.0f));
            break;
        }
        case CubicBezierCurveType::kCusp: {
            float ls = curve.d3;
            float lt = 3.0f * curve.d2;
            float lsMinusLt = ls - lt;

            tex_coordinates[0] = {ls, ls * ls * ls, 1.0f};
            tex_coordinates[1] = {ls - kOneThird * lt, ls * ls * lsMinusLt, 1.0f};
            tex_coordinates[2] = {ls - kTwoThirds * lt, lsMinusLt * lsMinusLt * ls, 1.0f};
            tex_coordinates[3] = {lsMinusLt, lsMinusLt * lsMinusLt * lsMinusLt, 1.0f};
            break;
        }
        case CubicBezierCurveType::kQuadratic: {
            tex_coordinates[0] = {0, 0, 0};
            tex_coordinates[1] = {kOneThird, 0, kOneThird};
            tex_coordinates[2] = {kTwoThirds, kOneThird, kTwoThirds};
            tex_coordinates[3] = {1.0f, 1.0f, 1.0f};
            if (curve.d3 < 0) { reverse_orientation = true; }
            break;
        }
        case CubicBezierCurveType::kLine:
        case CubicBezierCurveType::kPoint:
        default:
        return {};
    }

    if (fill_side == FillSide::kRight) { reverse_orientation = !reverse_orientation; }

    if (reverse_orientation) {
        for (int i = 0; i < 4; ++i) {
            tex_coordinates[i].x *= -1;
            tex_coordinates[i].y *= -1;
        }
    }

    std::vector<Vertex> return_result = {{control_points[0], colour, glm::vec4(tex_coordinates[0], 0)},
                                        {control_points[1], colour, glm::vec4(tex_coordinates[1], 0)}, 
                                        {control_points[2], colour, glm::vec4(tex_coordinates[2], 0)},
                                        {control_points[3], colour, glm::vec4(tex_coordinates[3], 0)}};                                 
    return return_result;
}

std::vector<Vertex> GenerateQuadBezierStroke(glm::vec2 control_points[3], glm::vec3 colour, float stroke_radius, std::vector<uint32_t>& indices) {
    // TODO: Get angle and tidy up
    glm::vec2 v1 = control_points[1] - control_points[0];
    glm::vec2 v2 = control_points[2] - control_points[1];
    glm::vec2 perp_vector_v1 = stroke_radius * glm::vec2(-v1.y, v1.x);
    glm::vec2 perp_vector_v2 = stroke_radius * glm::vec2(-v2.y, v2.x);
    auto p1a = control_points[0] + perp_vector_v1;
    auto p1b = control_points[0] - perp_vector_v1;
    auto c1a = control_points[1] + perp_vector_v1;
    auto c1b = control_points[1] - perp_vector_v1;
    auto p2a = control_points[2] + perp_vector_v2;
    auto p2b = control_points[2] - perp_vector_v2;
    auto c2a = control_points[1] + perp_vector_v2;
    auto c2b = control_points[1] - perp_vector_v2;
    glm::vec2 ncp1, ncp2;
    GetIntersectionPoint(p1a, c1a, p2a, c2a, ncp1);
    GetIntersectionPoint(p1b, c1b, p2b, c2b, ncp2);
    std::vector<std::array<glm::vec2, 3>> top_triangles = {{p1a, ncp1, p2a}};
    std::vector<std::array<glm::vec2, 3>> bot_triangles = {{p1b, ncp2, p2b}};
    std::set<std::size_t> top_split_values;
    std::set<std::size_t> bot_split_values;
    // Check on overlaps and split if needed
    bool has_split = true;
    while (has_split) {
        has_split = false;
        // Check for overlap
        for (std::size_t topIndex = 0; topIndex < top_triangles.size(); topIndex++) {
            for (std::size_t botIndex = 0; botIndex < bot_triangles.size(); botIndex++) {
                if (CheckTriangleCollision(top_triangles[topIndex], bot_triangles[botIndex])) {
                    // There is overlap so handle this case
                    if (GetTriangleArea(top_triangles[topIndex]) >= GetTriangleArea(bot_triangles[botIndex])) {
                        // Subdivide top triangle
                        auto newTriangles = SplitQuadraticBezier(top_triangles[topIndex], 0.5);
                        top_triangles[topIndex] = std::move(newTriangles[0]);
                        top_triangles.emplace(top_triangles.begin() + topIndex + 1, std::move(newTriangles[1]));
                        if (bot_split_values.find(topIndex) == bot_split_values.end()) {
                            top_split_values.emplace(topIndex);
                        } else {
                            bot_split_values.erase(topIndex);
                        }
                    } else {
                        // Subdivide bot triangle
                        auto newTriangles = SplitQuadraticBezier(bot_triangles[botIndex], 0.5);
                        bot_triangles[botIndex] = std::move(newTriangles[0]);
                        bot_triangles.emplace(bot_triangles.begin() + botIndex + 1, std::move(newTriangles[1]));
                        if (top_split_values.find(botIndex) == top_split_values.end()) {
                            bot_split_values.emplace(botIndex);
                        } else {
                            top_split_values.erase(botIndex);
                        }
                    }
                    has_split = true;
                    break;
                }
            }
            if (has_split) break;
        }
    }
    uint16_t count = 0;
    uint16_t top_tri_num = 0;
    uint16_t bot_tri_num = 0;
    std::vector<Vertex> return_list;
    auto side_colour = 1.0f;
    auto bot_tri_offset = top_triangles.size();
    top_triangles.insert(top_triangles.end(), bot_triangles.begin(), bot_triangles.end());
    std::vector<glm::vec3> texture_values = {{0.0f, 0.0f, 0.0f},
                                            {0.5f, 0.0f, 0.0f},
                                            {1.0f, 1.0f, 0.0f}};
    // Constructing the inner triangles (probably overcomplicating)
    for (auto& triangle : top_triangles) {
        for (auto& point : triangle) {
            return_list.emplace_back(graphics::Vertex({glm::vec3(point, 0), colour,  {0.0f, 0.0f, 0.0f}, side_colour, FillType::kTriangle}));
            count++;
        }
    }
    while (top_tri_num < bot_tri_offset) {
        bool top_found = (top_split_values.find(top_tri_num) != top_split_values.end());
        bool bot_found = (bot_split_values.find(bot_tri_num) != bot_split_values.end());
        if (top_found && !bot_found) {
            // Corresponds to two top triangles matching a bottom one (2 -> 1) 
            auto top_tri_1_index = top_tri_num * 3;
            auto top_tri_2_index = top_tri_1_index + 3;
            auto bot_tri_index = bot_tri_offset * 3 + (bot_tri_num * 3);
            indices.emplace_back(bot_tri_index);
            indices.emplace_back(top_tri_1_index);
            indices.emplace_back(top_tri_1_index + 2);
            indices.emplace_back(top_tri_1_index + 2);
            indices.emplace_back(bot_tri_index + 1);
            indices.emplace_back(bot_tri_index);
            indices.emplace_back(bot_tri_index + 1);
            indices.emplace_back(top_tri_2_index);
            indices.emplace_back(top_tri_2_index + 2);
            indices.emplace_back(top_tri_2_index + 2);
            indices.emplace_back(bot_tri_index + 2);
            indices.emplace_back(bot_tri_index + 1);
            top_tri_num += 2;
            bot_tri_num += 1;
        } else if (!top_found && bot_found) {
            // Corresponds to one top triangle matching two bot triangles (1 -> 2)
            // is this configuration possible...?
            auto top_tri_index = top_tri_num * 3;
            auto bot_tri_index_1 = bot_tri_offset * 3 + (bot_tri_num * 3);
            auto bot_tri_index_2 = bot_tri_offset * 3 + (bot_tri_num * 3) + 1;
            indices.emplace_back(bot_tri_index_1);
            indices.emplace_back(top_tri_index);
            indices.emplace_back(bot_tri_index_1 + 1);
            indices.emplace_back(top_tri_index);
            indices.emplace_back(top_tri_index + 2);
            indices.emplace_back(bot_tri_index_1 + 1);
            indices.emplace_back(bot_tri_index_1 + 1);
            indices.emplace_back(top_tri_index + 2);
            indices.emplace_back(bot_tri_index_2 + 1);
            indices.emplace_back(bot_tri_index_2 + 2);
            indices.emplace_back(bot_tri_index_2 + 1);
            indices.emplace_back(top_tri_index + 2);
            indices.emplace_back(bot_tri_index_1 + 1);
            indices.emplace_back(bot_tri_index_2 + 1);
            indices.emplace_back(bot_tri_index_2);
            top_tri_num += 1;
            bot_tri_num += 2;
        } else {
            // Corresponds to a 1 to 1 match
            auto top_tri_index = top_tri_num * 3;
            auto bot_tri_index = bot_tri_offset * 3 + (bot_tri_num * 3);
            indices.emplace_back(bot_tri_index);
            indices.emplace_back(top_tri_index);
            indices.emplace_back(bot_tri_index + 1);
            indices.emplace_back(top_tri_index);
            indices.emplace_back(top_tri_index + 2);
            indices.emplace_back(bot_tri_index + 1);
            indices.emplace_back(bot_tri_index + 1);
            indices.emplace_back(top_tri_index + 2);
            indices.emplace_back(bot_tri_index + 2);
            top_tri_num += 1;
            bot_tri_num += 1;
        }
    }
    // Set up the outer triangles
    auto switch_number = bot_tri_offset * 3 + count;
    for (auto& triangle : top_triangles) {
        if (count == switch_number) { side_colour *= -1.0f; }
        for (std::size_t i = 0; i < 3; i++) {
            return_list.emplace_back(graphics::Vertex({glm::vec3(triangle[i], 0), colour, texture_values[i], side_colour, FillType::kQuadratic}));
            indices.emplace_back(count++);
        }
    }
    return return_list;
}

bool GetIntersectionPoint(const glm::vec2 l1p1, const glm::vec2 l1p2, const glm::vec2 l2p1, const glm::vec2 l2p2, glm::vec2& return_value) {
    glm::vec3 abc1 = {l1p2.y - l1p1.y, l1p1.x - l1p2.x, ((l1p2.y - l1p1.y) * l1p1.x) + ((l1p1.x - l1p2.x) * l1p1.y)};
    glm::vec3 abc2 = {l2p2.y - l2p1.y, l2p1.x - l2p2.x, ((l2p2.y - l2p1.y) * l2p1.x) + ((l2p1.x - l2p2.x) * l2p1.y)};
    double determinant = abc1[0] * abc2[1] - abc2[0] * abc1[1];
    if (determinant == 0) { return false; }
    return_value.x = (abc2[1] * abc1[2] - abc1[1] * abc2[2]) / determinant;
    return_value.y = (abc1[0] * abc2[2] - abc2[0] * abc1[2]) / determinant;
    return true;
}

bool CheckLineSegmentIntersection(const glm::vec2 l1p1, const glm::vec2 l1p2, const glm::vec2 l2p1, const glm::vec2 l2p2) {
    glm::vec2 intersection_point;
    bool return_bool = false;
    if (GetIntersectionPoint(l1p1, l1p2, l2p1, l2p2, intersection_point)) {
        // Checks if point is on both line segements thus it intersects within the segments
        if (((intersection_point.x >= std::min(l1p1.x, l1p2.x) && intersection_point.x <= std::max(l1p1.x, l1p2.x)) &&
            (intersection_point.y >= std::min(l1p1.y, l1p2.y) && intersection_point.y <= std::max(l1p1.y, l1p2.y))) &&
            ((intersection_point.x >= std::min(l2p1.x, l2p2.x) && intersection_point.x <= std::max(l2p1.x, l2p2.x)) &&
            (intersection_point.y >= std::min(l2p1.y, l2p2.y) && intersection_point.y <= std::max(l2p1.y, l2p2.y)))) { return_bool = true; }
    }
    return return_bool;
}

std::vector<std::array<glm::vec2, 3>> SplitQuadraticBezier(const std::array<glm::vec2, 3> bezier_curve, float z) {
    std::vector<std::array<glm::vec2, 3>> return_vector(2);
    // First Bezier
    return_vector[0][0] = bezier_curve[0];
    return_vector[0][1] = {z * bezier_curve[1].x - (z - 1) * bezier_curve[0].x, z * bezier_curve[1].y - (z - 1) * bezier_curve[0].y };
    return_vector[0][2] = {pow(z, 2) * bezier_curve[2].x - 2 * z * (z - 1) * bezier_curve[1].x + pow((z - 1), 2) * bezier_curve[0].x, 
                          pow(z, 2) * bezier_curve[2].y - 2 * z * (z - 1) * bezier_curve[1].y + pow((z - 1), 2) * bezier_curve[0].y};
    return_vector[1][0] = return_vector[0][2];
    return_vector[1][1] = {z * bezier_curve[2].x - (z - 1) * bezier_curve[1].x, z * bezier_curve[2].y - (z - 1) * bezier_curve[1].y};
    return_vector[1][2] = bezier_curve[2];
    // Second Bezier
    return return_vector;
}

bool CheckTriangleCollision(const std::array<glm::vec2, 3> triangle1, const std::array<glm::vec2, 3> triangle2) {
    for (int i = 0; i < 3; i++) {
        if (CheckLineSegmentIntersection(triangle1[i], triangle1[(i + 1) % 3], triangle2[0], triangle2[1])) { return true;}
        if (CheckLineSegmentIntersection(triangle1[i], triangle1[(i + 1) % 3], triangle2[1], triangle2[2])) { return true;}
        if (CheckLineSegmentIntersection(triangle1[i], triangle1[(i + 1) % 3], triangle2[2], triangle2[0])) { return true;}
    }
    // Check for points in triangle (triangle contains another triangle for example)
    // If no edges intersect then all 3 points must be within a triangle for it to 'overlap'
    // If triangles are equivalent
    // The first checks if t1 is in t2 and the second for t2 in t1
    return CheckPointInTriangle(triangle1[0], triangle2) || CheckPointInTriangle(triangle2[0], triangle1);
}

float GetTriangleArea(const std::array<glm::vec2, 3> triangle) {
    glm::vec3 edge1(triangle[2] - triangle[0], 0);
    glm::vec3 edge2(triangle[1] - triangle[0], 0);
    return abs(0.5 * (glm::length(glm::cross(edge1, edge2))));
}

bool CheckPointInTriangle(const glm::vec2 point, const std::array<glm::vec2, 3> triangle) {
    // Some error could be involved with straight lines and points on edge
    std::array<glm::vec2, 3> t1 = {triangle[0], triangle[1], point};
    std::array<glm::vec2, 3> t2 = {triangle[1], triangle[2], point};
    std::array<glm::vec2, 3> t3 = {triangle[2], triangle[0], point};
    // Assuming round error is +- 
    return (abs((GetTriangleArea(t1) + GetTriangleArea(t2) + GetTriangleArea(t3)) - GetTriangleArea(triangle)) <= kRoundError);
}

}