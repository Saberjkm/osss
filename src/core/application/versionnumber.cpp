#include "application/versionnumber.h"

#include <iostream>
namespace application {

VersionNumber CreateVersionNumber(std::string version_number) {
    VersionNumber return_version;
    if (std::regex_match(version_number, kVersionMatcher)) {
        return_version.set_version_type(version_number.at(version_number.size() - 1));
        std::regex split_regex("(0|[1-9][0-9]*)"); // Needs to outlive iterator
        auto words_begin = std::sregex_iterator(version_number.begin(), version_number.end(), split_regex);
        auto words_end = std::sregex_iterator();
        if (std::distance(words_begin, words_end) == 3) {
            std::string major_number = (*(words_begin++)).str();
            std::string minor_number = (*(words_begin++)).str();
            std::string patch_number = (*(words_begin++)).str();
            // Rather than doing an entire change all at once,
            // splitting it into sections like this allows for error checking from within the v.number
            // i.e an error in the minor_number would mean that it would be equal to -1 within the v.number.
            // The method is_valid() should be used to check if the entire v.number is actually valid
            try { return_version.set_major_number(std::stoul(major_number)); } catch(...) {} 
            try { return_version.set_minor_number(std::stoul(minor_number)); } catch(...) {} 
            try { return_version.set_patch_number(std::stoul(patch_number)); } catch(...) {} 
        }
        return return_version;
    }
    return return_version;
}

}