#ifndef VVV_MOCK_SYSTEMREGISTER_H_INCLUDED
#define VVV_MOCK_SYSTEMREGISTER_H_INCLUDED

#include "gmock/gmock.h"

#include "systems/systemregister.h"

namespace systems {
    class MockSystemRegister : public SystemRegister {
        public:
        ~MockSystemRegister(){}
        MockSystemRegister() = delete;
        MockSystemRegister(SystemRegistrar& parent_registrar, std::string register_name) : SystemRegister(parent_registrar, register_name) {}

        MOCK_METHOD(std::weak_ptr<AbstractSystem>, GetSystem, (std::string system_label_id), (override));
    };
}
#endif