#include "graphics/renderer.h"
#include "graphics/graphicsregistrar.h"

namespace graphics {

//-- Renderer
    void Renderer::CleanUp() {
        Wait();
        for (auto& command_pool : command_pools_) {
            vkDestroyCommandPool(parent_registrar_.device(), command_pool, nullptr);
        }
    }
//--

}