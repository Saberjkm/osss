#include <stdlib.h>

#include "test/test.h"

namespace test {

float WeakRandomChance() {
    return static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
}

unsigned long GetRandomLowInt() {
    return rand() % kLowNumber;
}

unsigned long GetRandomMedInt() {
    return rand() % kMedNumber;
}

unsigned long GetRandomHghInt() {
    return rand() % kHghNumber;
}

}