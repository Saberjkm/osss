#ifndef VVV_ENTITYREGISTER_H_INCLUDED
#define VVV_ENTITYREGISTER_H_INCLUDED

#include <string>
#include <map>
#include <memory>

#include "application/application.h"
#include "entities/entities.h"

namespace entities {
class EntityRegistrar;

struct EntityIndex {
    std::string scene_label;
    std::string entity_label;

    bool operator<(const EntityIndex& other_index) const {
        return ((scene_label < other_index.scene_label) && (entity_label < other_index.entity_label)); 
    }
};

class EntityRegister {
    protected:
    
    EntityRegistrar& parent_registrar_;
    std::string register_label_;

    std::map<EntityIndex, std::shared_ptr<EntityStorageData>> entity_map;

    public:

    EntityRegister() = delete;
    EntityRegister(EntityRegistrar& parent_registrar, std::string register_label);

    /**
     * @brief Get the data for an Entity that is represented by the given representation
     * 
     * @param entity The representation of the Entity you want the data for
     * @return EntityStorageData The data related to the Entity you want
     */
    std::shared_ptr<const EntityStorageData> GetEntityData(const application::EntityRepresentation& entity);

    /**
     * @brief Gets a a list of the component types present for a given entity
     * 
     * @param scene_label The scene the entity is a part of
     * @param entity_label The entity's ID
     * @return std::vector<application::ComponentTypeRepresentation> The list of component types present
     */
    std::vector<application::ComponentTypeRepresentation> GetComponentTypes(std::string scene_label, std::string entity_label);
};

}

#endif