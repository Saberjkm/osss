#ifndef VVV_VVVSYSTEMREGISTER_H_INCLUDED
#define VVV_VVVSYSTEMREGISTER_H_INCLUDED

#include "systems/systemregister.h"
#include "systems/systems.h"
#include "systems/vvv2dgraphics.h"

namespace systems {
class VVV2DGraphics;

class VVVSystemRegister: public SystemRegister {
    protected:
    std::shared_ptr<VVV2DGraphics> graphics_system;

    public:
    VVVSystemRegister(SystemRegistrar& parent_registrar, std::string register_name) 
        : SystemRegister(parent_registrar, register_name),
          graphics_system(std::make_shared<VVV2DGraphics>(parent_registrar_.global_context(), parent_registrar_.logic_registrar())) 
    {
        core_systems_[kVVV2DGraphicsSystemName] = graphics_system;
    }
    
    ~VVVSystemRegister() {}
};

}
#endif