#include "gtest/gtest.h"

#include "test/mock_console.h"
#include "test/mock_application.h"
#include "test/mock_logicregistrar.h"
#include "entities/entityregistrar.h"

#include <iostream>

using ::testing::_;
using ::testing::Return;

class EntityRegistrarTest : public ::testing::Test {
    protected:
    console::MockSystemConsole mock_console_;
    application::MockApplication mock_global_context_;
    logic::MockLogicRegistrar mock_logic_registrar_;
    entities::EntityRegistrar ert1_;
    components::MockComponentRegistrar mock_component_registrar_;
    systems::MockSystemRegistrar mock_system_registrar_;
    EntityRegistrarTest() 
        : mock_global_context_(mock_console_),
          mock_logic_registrar_(mock_global_context_, ert1_, mock_component_registrar_, mock_system_registrar_),
          ert1_(mock_global_context_, mock_logic_registrar_), 
          mock_component_registrar_(mock_global_context_, mock_logic_registrar_),
          mock_system_registrar_(mock_global_context_, mock_logic_registrar_) 
    {}

};
