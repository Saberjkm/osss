#ifndef VVV_XML_H_INCLUDED
#define VVV_XML_H_INCLUDED

#include <filesystem>
#include <bitset>


#include "tinyxml2/tinyxml2.h"
#include "application/application.h"
#include "application/versionnumber.h"

namespace logic { class LogicRegistrar; }
namespace application { class BaseApplication; }
namespace scenes { struct SceneData; }

namespace application::xml {
    enum class FileType {
        kGeneric, /* The type for a file that doesn't fall under any category */
        kScene    /* A file representing scene info */
    };

    FileType ConvertToFileType(std::string type);

    struct XMLElementList {
        tinyxml2::XMLElement* parent_node;
        std::vector<tinyxml2::XMLElement*> list;
    };

    struct XMLHeader {
        std::filesystem::path file_location;
        std::string id = "";
        bool is_valid = false;
        FileType file_type = FileType::kGeneric;
        VersionNumber file_version;

        bool operator==(const XMLHeader& rhs) const {
            return this->file_location == rhs.file_location   &&
                   this->id == rhs.id                         &&
                   this->is_valid == rhs.is_valid             &&
                   this->file_type == rhs.file_type           &&
                   this->file_version == rhs.file_version;
        }
    };

    struct SceneParseData {
        tinyxml2::XMLNode* root_node;
        tinyxml2::XMLNode* body_node;
        tinyxml2::XMLNode* load_node;
        tinyxml2::XMLNode* data_node;
        // Load Node
        tinyxml2::XMLNode* component_types_node;
        std::vector<tinyxml2::XMLElement*> component_type_nodes;
        tinyxml2::XMLNode* systems_node;
        std::vector<tinyxml2::XMLElement*> system_nodes;
        // Data Node
        tinyxml2::XMLNode* entities_node;
        std::vector<std::pair<tinyxml2::XMLElement*, std::vector<XMLElementList>>> entities;
    };

    class XMLFile {
        protected:
        application::BaseApplication& global_context_;


        bool has_loaded_ = false;
        tinyxml2::XMLDocument xml_document_;

        XMLHeader file_header_;

        std::size_t GetAttributeMask(tinyxml2::XMLElement* node);
        
        /**
         * @brief Loads the component types into the relevant registrar
         * @param related_logic_registrar The logic registrar to load into
         * @param component_type_nodes The list of component types in the file
         * @param scene_component_types The struct which contains the info about types loaded
         * @return true All loaded successfully 
         * @return false At Least one type was not loaded successfully
         */
        bool LoadComponentTypes(logic::LogicRegistrar& related_logic_registrar, 
                                const std::vector<tinyxml2::XMLElement*>& component_type_nodes,  
                                std::vector<application::ComponentTypeData>& component_types_storage);

        /**
         * @brief Loads the systems into the relevant registrar
         * @param related_logic_registrar 
         * @param system_nodes 
         * @param scene_systems 
         * @return true 
         * @return false 
         */
        bool LoadSystems(logic::LogicRegistrar& related_logic_registrar, 
                         const std::vector<tinyxml2::XMLElement*>& system_nodes,  
                         std::vector<application::SystemData>& systems_storage);


        bool LoadEntities(logic::LogicRegistrar& related_logic_registrar, 
                          const std::vector<std::pair<tinyxml2::XMLElement*, std::vector<XMLElementList>>>& entity_nodes,  
                          std::map<std::string, application::EntityData>& entities_storage);

        public:
        
        XMLFile() = delete;

        XMLFile(application::BaseApplication& global_context, std::string file_location) 
            : global_context_(global_context)
        {
            file_header_.file_location = std::filesystem::path(file_location).lexically_normal();
        }

        XMLFile(application::BaseApplication& global_context, std::filesystem::path file_location) 
            : global_context_(global_context)
        {
            file_header_.file_location = file_location.lexically_normal();
        }
        
        bool has_loaded() { return has_loaded_; }
        bool has_header() { return file_header_.is_valid; }
        std::filesystem::path file_location() { return file_header_.file_location; }
        const XMLHeader& header() { return file_header_; }

        bool LoadHeader();

        scenes::SceneData LoadScene(logic::LogicRegistrar& related_logic_registrar);

    };

}

#endif