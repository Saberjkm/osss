
#include "entities/entityregistrar.h"
#include "entities/entity.h"
#include "entities/entities.h"
#include "entities/entitysearchtree.h"
#include "entities/entityregister.h"
#include "entities/vvventityregister.h"
#include "logic/logicregistrar.h"
#include "application/console.h"
#include "application/application.h"
#include "scenes/scenes.h"

namespace entities {

//-- EntityRegistrar Class
EntityRegistrar::EntityRegistrar(application::BaseApplication& global_context, logic::LogicRegistrar& related_logic_registrar)
        : global_context_(global_context), 
          related_logic_registrar_(related_logic_registrar)
{
    core_entity_register_ = std::make_shared<VVVEntityRegister>(*this, kVVVCoreEntityRegisterName);
    entity_registers_.emplace(kVVVCoreEntityRegisterName, core_entity_register_);
}

components::ComponentRegistrar& EntityRegistrar::related_component_registrar() {
    return related_logic_registrar_.component_registrar();
}

application::EntityData EntityRegistrar::CreateNewEntity(const entities::EntityStorageData& entity, const application::EntityRepresentation& entity_rep) {
    // Check if the components are unique
    bool all_unique = true;
    std::set<std::string> unique_ids; 
    for (auto& component : entity.components) {
        if (unique_ids.contains(component.component_label)) {
            all_unique = false;
            break;
        }
        unique_ids.emplace(component.component_label);
    }
    if (!all_unique) return {};

    // Create the entity
    auto entity_data = CreateNewEntity(entity_rep);
    if (entity_data.id != (std::size_t) -1) {
        std::string component_error;
        auto& entity_pointer = entity_list_[entity_data.id]; 
        for (auto& component : entity.components) {
            if (!entity_pointer->AddComponent(component.component_label, related_component_registrar().CreateNewComponent(component.component_label, component.component_value))) {
                component_error = component.component_label;
                break;
            }
        }
        if (component_error.size()) {
            global_context_.main_console().IssueWarning(
                "entities::EntityRegistrar::CreateEntity(const entities::EntityStorageData&)", 
                "Component Creation Error",
                fmt::format("Couldn't create a Component of Label {} for the new entity.", component_error)
            );
            UndoCreation(entity_data);
            return {};
        }
    } else {
        global_context_.main_console().IssueWarning(
            "entities::EntityRegistrar::CreateEntity(const entities::EntityStorageData&)", 
            "Entity Creation Error",
            "Couldn't create a new entity."
        );
        return {};
    }
    return entity_data;
}

application::EntityData EntityRegistrar::CreateNewEntity(const application::EntityRepresentation& entity_rep) {
    application::EntityData return_entity_data;
    if (current_frame_.deleted_values.size() > 0) {
        return_entity_data.id = current_frame_.deleted_values.back();
        current_frame_.deleted_values.pop_back();
        entity_list_[return_entity_data.id]->Load(entity_rep.flag_data);
    } else {
        return_entity_data.id = entity_list_.size();
        entity_list_.emplace_back(std::make_shared<Entity>(return_entity_data.id, global_context_, *this));
        entity_list_[return_entity_data.id]->Load(entity_rep.flag_data);
    }
    if (return_entity_data.id != -1) return_entity_data.is_valid = true;
    return_entity_data.entity_label = entity_rep.entity_label;
    return_entity_data.flag_data = entity_rep.flag_data;
    next_frame_.created_this_frame.emplace(return_entity_data.id);
    return return_entity_data;
}

bool EntityRegistrar::Initialise(const application::EntityRegistrarSettings& entity_registrar_settings) {
    return true;
}

bool EntityRegistrar::FirstFrame() {
    return true;
}

EntitySearchTree* EntityRegistrar::InsertKey(std::size_t entity_key, const std::map<std::string, std::pair<std::size_t, ComponentAccess>>* component_data) {
    return (search_root_.InsertKey(entity_key, component_data));
}

bool EntityRegistrar::HasEntity(std::size_t entity_id) {
    if (entity_id != (std::size_t) -1 && entity_list_.size() > entity_id) {
        return used_space_[entity_id];
    } else {
        return false;
    }
}

bool EntityRegistrar::HasRegister(std::string register_label) {
    return entity_registers_.contains(register_label);
}

bool EntityRegistrar::WillHaveEntity(std::size_t entity_id) {
    if (entity_id != (std::size_t) -1 && entity_list_.size() > entity_id && !next_frame_.deleted_this_frame.contains(entity_id)) {
        return used_space_[entity_id];
    } else {
        return false;
    }
}

std::shared_ptr<Entity> EntityRegistrar::operator[](std::size_t entity_id) {
    if (HasEntity(entity_id)) {
        return entity_list_[entity_id];
    } else {
        return nullptr;
    }
}

std::vector<std::size_t> EntityRegistrar::GatherEntities(const std::set<std::string>& component_labels) {
    return search_root_.GatherEntities(component_labels);
}

std::vector<std::shared_ptr<Entity>> EntityRegistrar::GatherEntityPointers(const std::set<std::string>& component_labels) {
    std::vector<std::shared_ptr<Entity>> return_list;
    for (auto id : GatherEntities(component_labels)) {
        if (HasEntity(id)) { return_list.emplace_back(entity_list_[id]); }
    }
    return return_list;
}

std::vector<std::shared_ptr<Entity>> EntityRegistrar::GatherEntityPointers(const std::set<std::string>& component_labels, std::function<bool(const std::shared_ptr<entities::Entity>)> gather_condition) {
    std::vector<std::shared_ptr<Entity>> return_list;
    for (auto id : GatherEntities(component_labels)) {
        if (HasEntity(id) && gather_condition(entity_list_[id])) { return_list.emplace_back(entity_list_[id]); }
    }
    return return_list;
}

std::map<std::size_t, std::map<std::string, std::pair<std::size_t, ComponentAccess>>> EntityRegistrar::GatherEntityData(const std::set<std::string>& component_labels) {
    std::map<std::size_t, std::map<std::string, std::pair<std::size_t, ComponentAccess>>> return_map;
    for (auto id : GatherEntities(component_labels)) {
        auto entity_data = entity_list_[id]->GetDataRepresentation(component_labels);
        return_map[entity_data.first] = entity_data.second;
    }
    return return_map;
}

std::weak_ptr<Entity> EntityRegistrar::GetWeakPointer(std::size_t entity_id) {
    if (HasEntity(entity_id)) {
        return std::weak_ptr<Entity>(entity_list_[entity_id]);
    }
    return {};
}

std::shared_ptr<Entity> EntityRegistrar::QueryEntity(std::size_t entity_id) {
    if (WillHaveEntity(entity_id)) {
        return entity_list_[entity_id];
    }
    return nullptr;
}

void EntityRegistrar::AddCreationCallback(std::pair<std::string, std::function<void(std::size_t)>> callback) {
    if (!creation_callbacks_.contains(callback.first)) {
        creation_callbacks_[callback.first] = std::move(callback.second);
        return;
    } else {
        global_context_.main_console().IssueWarning(
            "entities::EntityRegistrar::AddCreationCallback(std::pair<std::string, std::function<void(std::size_t, bool)>>)", 
            "Bad Callback ID",
            fmt::format("Callback for ID {} already exists", callback.first)
        );
        return;
    }
}

bool EntityRegistrar::DeleteCreationCallback(std::string callback_id) {
    creation_callbacks_.erase(callback_id);
    return (!creation_callbacks_.contains(callback_id));
}

bool EntityRegistrar::AdvanceFrame() {
    current_frame_ = next_frame_;
    next_frame_.created_this_frame.clear();
    next_frame_.deleted_this_frame.clear();
    bool advanced_successfully = true;
    for (auto& entity : entity_list_) {
        if (!entity->AdvanceFrame()) { advanced_successfully = false;}
    }
    search_root_.AdvanceFrame();
    return advanced_successfully;
}

bool EntityRegistrar::EndOfFrame() {
    if (used_space_.size() != entity_list_.size()) used_space_.resize(entity_list_.size(), false);
    for (auto deleted_value : next_frame_.deleted_this_frame) {
        if (specific_deletion_callbacks_.contains(deleted_value)) {
            for (auto& deletion_event : specific_deletion_callbacks_[deleted_value]) {
                deletion_event.second(deleted_value);
            }
        }
        for (auto& deletion_event : general_deletion_callbacks_) {
            deletion_event.second(deleted_value);
        }
        used_space_[deleted_value] = false;
    }
    for (auto& created_value : next_frame_.created_this_frame) {
        for (auto& creation_event : creation_callbacks_) {
            creation_event.second(created_value);
        }
        used_space_[created_value] = true;
    }
    for (auto& entity : entity_list_) {
        entity->EndOfFrame();
    }
    return true;
}

application::EntityData EntityRegistrar::CreateEmptyEntity() {
    application::EntityData return_entity_data;
    if (current_frame_.deleted_values.size() > 0) {
        return_entity_data.id = current_frame_.deleted_values.back();
        current_frame_.deleted_values.pop_back();
        entity_list_[return_entity_data.id]->Load(application::kNoFlags);
    } else {
        return_entity_data.id = entity_list_.size();
        entity_list_.emplace_back(std::make_shared<Entity>(return_entity_data.id, global_context_, *this));
        entity_list_[return_entity_data.id]->Load(application::kNoFlags);
    }
    if (return_entity_data.id != -1) return_entity_data.is_valid = true;
    next_frame_.created_this_frame.emplace(return_entity_data.id);
    return return_entity_data;
}

application::EntityData EntityRegistrar::CreateEntity(const application::EntityRepresentation& entity) {
    if (entity_registers_.contains(entity.register_label)) {
        application::EntityData return_entity_data;
        auto entity_data_pointer = entity_registers_[entity.register_label]->GetEntityData(entity);
        if (!entity_data_pointer) {
            global_context_.main_console().IssueWarning(
                "entities::EntityRegistrar::CreateEntity(const application::EntityRepresentation&)", 
                "Invalid Entity",
                fmt::format("Couldn't create Entity from Register {}, with Scene ID {}, and Entity ID {}", entity.register_label, entity.scene_label, entity.entity_label)
            );
            return {};
        }
        return_entity_data= CreateNewEntity(*entity_data_pointer, entity);
        if (!return_entity_data.is_valid) {
            return {};
        }
        return return_entity_data;
    } else {
        global_context_.main_console().IssueWarning(
            "entities::EntityRegistrar::CreateEntity(const application::EntityRepresentation&)", 
            "Invalid Register",
            fmt::format("Register doesn't exist with Label {}", entity.register_label)
        );
        return {};
    }
}

application::EntityData EntityRegistrar::CreateReferenceEntity(std::size_t target_entity_id) {
    application::EntityData return_entity_data;
    if (HasEntity(target_entity_id)) {
        if (WillHaveEntity(target_entity_id)) {
            return_entity_data = CreateEmptyEntity();
            if (!return_entity_data.is_valid) {
                global_context_.main_console().IssueWarning(
                    "entities::EntityRegistrar::CreateReferenceEntity(std::size_t)", 
                    "Creation Error",
                    "Couldn't create a new entity."
                );
                return {};
            }
            return_entity_data.entity_label = "Copy_Of_" + std::to_string(target_entity_id);
            entity_list_[return_entity_data.id]->Reference(entity_list_[target_entity_id]);    
        } else {
            global_context_.main_console().IssueWarning(
                "entities::EntityRegistrar::CreateReferenceEntity(std::size_t)", 
                "Invalid Entity ID",
                fmt::format("Entity will not exist with ID {}", target_entity_id)
            );
        }
    } else {
        global_context_.main_console().IssueWarning(
            "entities::EntityRegistrar::CreateReferenceEntity(std::size_t)", 
            "Invalid Entity ID",
            fmt::format("Entity doesn't exist with ID {}", target_entity_id)
        );
    }
    return return_entity_data;
}

bool EntityRegistrar::DeleteEntity(std::size_t entity_id) {
    if (HasEntity(entity_id)) {
        if (WillHaveEntity(entity_id)) {
            next_frame_.deleted_this_frame.emplace(entity_id);
            next_frame_.deleted_values.push_back(entity_id);
            entity_list_[entity_id]->Clear();
        }
        return true;
    } else {
        global_context_.main_console().IssueWarning(
            "entities::EntityRegistrar::DeleteEntity(std::size_t)", 
            "Invalid Entity ID",
            fmt::format("Entity doesn't exist with ID {}", entity_id)
        );
        return false;
    }
}

bool EntityRegistrar::UndoCreation(const application::EntityData& entity) {
    if (HasEntity(entity.id)) {
        global_context_.main_console().IssueWarning(
            "entities::EntityRegistrar::UndoCreation(std::size_t)", 
            "Invalid Entity ID",
            fmt::format("Trying to undo creation of an entity that already exists ID {}", entity.id)
        );
        return false;
    }
    if (WillHaveEntity(entity.id)) {
        return entity_list_[entity.id]->UndoCreation(entity);
    } else {
        global_context_.main_console().IssueWarning(
            "entities::EntityRegistrar::UndoCreation(std::size_t)", 
            "Invalid Entity ID",
            fmt::format("Trying to undo creation of an entity that was not created with ID {}", entity.id)
        );
        return false;
    }
}

bool EntityRegistrar::UndoDeletion(std::size_t entity_id) {
    return true;
}

bool EntityRegistrar::UndoChanges(std::size_t entity_id) {
    return true;
}

std::vector<application::ComponentTypeRepresentation> EntityRegistrar::GetComponentTypesFromRegister(std::string register_label, std::string scene_label, std::string entity_label) {
    if (HasRegister(register_label)) {
        return entity_registers_[register_label]->GetComponentTypes(scene_label, entity_label);
    } else {
        global_context_.main_console().IssueWarning(
            "entities::EntityRegistrar::GetComponentTypesFromRegister(std::string, std::string, std::string)", 
            "Invalid Register Label",
            fmt::format("Register doesn't exist with ID {}", register_label)
        );
    }
    return {};
}

//-- End of EntityRegistrar Class
}