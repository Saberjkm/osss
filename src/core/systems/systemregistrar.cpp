#include "systems/systemregistrar.h"
#include "systems/systemregister.h"
#include "application/gameapplication.h"
#include "systems/system.h"
#include "input/input.h"
#include "systems/vvvsystemregister.h"

namespace systems {

SystemRegistrar::SystemRegistrar(application::BaseApplication& global_context, logic::LogicRegistrar& related_logic_registrar)
        : global_context_(global_context),
          related_logic_registrar_(related_logic_registrar),
          core_system_register_(std::make_shared<VVVSystemRegister>(*this, kVVVSystemRegisterName))
{
    system_registers_[kVVVSystemRegisterName] = core_system_register_;
}

bool SystemRegistrar::SystemDataHasChanged(std::string system_label) {
    if (HasSystem(system_label)) {
        return system_list_[system_label].lock()->DataHasChanged();
    }
    return false;
}

bool SystemRegistrar::SystemDataWillChange(std::string system_label) {
    if (HasSystem(system_label)) {
        return system_list_[system_label].lock()->DataWillChange();
    }
    return false;
}

std::shared_ptr<systems::SystemRegister> SystemRegistrar::GetSystemRegister(std::string label) {
    if (system_registers_.contains(label)) {
        return system_registers_[label];
    } else {
        global_context_.main_console().IssueWarning(
            "systems::BaseSystemRegistrar::GetSystemRegister(std::string)", 
            "Invalid System Register",
            fmt::format("Could not find System Register with Label {}", label)
        );
    }
    return {};
}

bool SystemRegistrar::AdvanceFrame() {
    for (auto& system : system_list_) {
        if (!system.second.expired()) {
            if (!system.second.lock()->AdvanceFrame()) {
                global_context_.main_console().IssueWarning(
                    "systems::SystemRegistrar::AdvanceFrame()", 
                    "Operation did not succeed",
                    fmt::format("System with ID {} failed its Advance Frame event", system.second.lock()->get_id())
                );
                return false;
            }
        }
    }
    return true;
}

bool SystemRegistrar::EndOfFrame() {
    for (auto& system : system_list_) {
        if (!system.second.expired()) {
            if (!system.second.lock()->EndOfFrame()) {
                global_context_.main_console().IssueWarning(
                    "systems::SystemRegistrar::EndOfFrame()", 
                    "Operation did not succeed",
                    fmt::format("System with ID {} failed its End Of Frame event", system.second.lock()->get_id())
                );
                return false;
            }
        }
    }
    return true;
}

bool SystemRegistrar::Initialise(const application::SystemRegistrarSettings& system_registrar_settings) {
    for (auto& system_register : system_registrar_settings.system_registers) {
        if (!system_registers_.contains(system_register.first)) {
            if (!system_register.second) {
                system_registers_.emplace(system_register.first, system_register.second);
            }
        } else {
            global_context_.main_console().IssueWarning(
                "systems::SystemRegistrar::Initialise(const application::SystemRegistrarSettings& )", 
                "Invalid Register",
                fmt::format("Can't add Register {} to the registrar as a register with the same name already existst.", system_register.first)
            );
        }
    }
    for (auto& system_register : system_registers_) {
        system_register.second->RegisterAllSystems();
    }
    return InitialiseImpl(system_registrar_settings);
}

bool SystemRegistrar::FirstFrame() {
    return true;
}

bool SystemRegistrar::RegisterSystem(std::string register_label, std::string system_label) {
    if (!HasSystem(system_label)) {
        if (system_registers_.find(register_label) != system_registers_.end()) {
            if (auto system = system_registers_[register_label]->GetSystem(system_label).lock()) {
                if (!system->RegisterID(system_list_.size())) {
                    global_context_.main_console().IssueWarning(
                        "systems::SystemRegistrar::RegisterSystem(std::string, std::string)", 
                        "Invalid ID",
                        fmt::format("Cant register System Label {} with the ID {}", system_label, system_list_.size())
                    );
                    return false;  
                }
                system_list_.emplace(system_label, std::move(system));
            } else {
                global_context_.main_console().IssueWarning(
                    "systems::SystemRegistrar::RegisterSystem(std::string, std::string)", 
                    "Invalid System Label",
                    fmt::format("System doesn't exist for {} with Label {}", register_label, system_label)
                );
                return false;
            }
        } else {
            global_context_.main_console().IssueWarning(
                "systems::SystemRegistrar::RegisterSystem(std::string, std::string)", 
                "Invalid Register Label",
                fmt::format("Register {} with Label {} isn't within this registrar.", register_label, system_label)
            );
            return false;
        }
    }
    return true;
}

bool SystemRegistrar::RegisterSystem(std::string system_label, std::weak_ptr<AbstractSystem> system_pointer) {
    if (!HasSystem(system_label)) {
        if (!system_pointer.expired()) {
            system_list_.emplace(system_label, std::move(system_pointer));
            return true;
        } else {
            global_context_.main_console().IssueWarning(
                "systems::SystemRegistrar::RegisterSystem(std::string, std::weak_ptr<AbstractSystem>)", 
                "Invalid System Pointer",
                fmt::format("System {} doesn't have a valid pointer", system_label)
            );
            return false;
        }
    }
    return true;
}

application::SystemData SystemRegistrar::LoadSystem(const application::SystemRepresentation& system) {
    application::SystemData return_system_data;
    if (HasSystem(system.system_label)) {
        if (auto system_pointer = system_list_[system.system_label].lock()) {
            if (!(return_system_data.is_valid = system_pointer->Load(system.flag_data))) {
                return_system_data.flag_data = system.flag_data;
                return_system_data.label = system.system_label;
                return_system_data.is_valid = true;
            } else {
                global_context_.main_console().IssueWarning(
                    "systems::SystemRegistrar::LoadSystem(application::SystemRe presentation)", 
                    "Invalid Load",
                    fmt::format("Trying to load a system with ID {}", system.system_label)
                );
            }
        }
    } else {
        global_context_.main_console().IssueWarning(
            "systems::SystemRegistrar::LoadSystem(application::SystemRepresentation)", 
            "Invalid ID",
            fmt::format("Trying to load a system that doesn't exist with ID {}", system.system_label)
        );
    }
    return return_system_data;
}

bool SystemRegistrar::UndoLoadSystem(const application::SystemData& system) {
    if (HasSystem(system.label)) {
        if (auto system_pointer = system_list_[system.label].lock()) {
            return system_pointer->UndoLoad(system.flag_data);
        }
    }
    global_context_.main_console().IssueWarning(
        "systems::SystemRegistrar::UndoLoadSystem(std::string)", 
        "Invalid ID",
        fmt::format("Trying to undo the load of a system that doesn't exist with ID {}", system.label)
    );
    return false;
}

bool SystemRegistrar::UnloadSystem(std::string system_label) {
    if (HasSystem(system_label)) {
        if (auto system = system_list_[system_label].lock()) {
            return system->Unload();
        }
    } 
    global_context_.main_console().IssueWarning(
        "systems::SystemRegistrar::UnloadSystem(std::string)", 
        "Invalid ID",
        fmt::format("Trying to unload a system that doesn't exist with ID {}", system_label)
    );
    return false;
}

bool SystemRegistrar::UndoUnloadSystem(std::string system_label) {
    if (HasSystem(system_label)) {
        if (auto system = system_list_[system_label].lock()) {
            return system->UndoUnload();
        }
    } 
    global_context_.main_console().IssueWarning(
        "systems::SystemRegistrar::UndoUnloadSystem(std::string)", 
        "Invalid ID",
        fmt::format("Trying to undo the unload of a system that doesn't exist with ID {}", system_label)
    );
    return false;
}

bool SystemRegistrar::LockSystem(std::string system_label, std::string lock_name) {
    if (HasSystem(system_label)) {
        if (auto system = system_list_[system_label].lock()) {
            return system->AddLock(lock_name);
        }
    } 
    global_context_.main_console().IssueWarning(
        "systems::SystemRegistrar::LockSystem(std::string, std::string)", 
        "Invalid ID",
        fmt::format("Trying to add a Lock {} to a system that doesn't exist with ID {}", lock_name, system_label)
    );
    return false;
}

bool SystemRegistrar::UndoLockSystem(std::string system_label, std::string lock_name) {
    if (HasSystem(system_label)) {
        if (auto system = system_list_[system_label].lock()) {
            return system->UndoAddLock(lock_name);
        }
    } 
    global_context_.main_console().IssueWarning(
        "systems::SystemRegistrar::UndoLockSystem(std::string, std::string)", 
        "Invalid ID",
        fmt::format("Trying to undo the addition of a Lock {} for a system that doesn't exist with ID {}", lock_name, system_label)
    );
    return false;
}

bool SystemRegistrar::UnlockSystem(std::string system_label, std::string lock_name) {
    if (HasSystem(system_label)) {
        if (auto system = system_list_[system_label].lock()) {
            return system->Unlock(lock_name);
        }
    } 
    global_context_.main_console().IssueWarning(
        "systems::SystemRegistrar::UnlockSystem(std::string, std::string)", 
        "Invalid ID",
        fmt::format("Trying to unlock the Lock {} for a system that doesn't exist with ID {}", lock_name, system_label)
    );
    return false;
}

bool SystemRegistrar::UndoUnlockSystem(std::string system_label, std::string lock_name) {
    if (HasSystem(system_label)) {
        if (auto system = system_list_[system_label].lock()) {
            return system->UndoUnlock(lock_name);
        }
    } 
    global_context_.main_console().IssueWarning(
        "systems::SystemRegistrar::UndoUnlockSystem(std::size_t, std::string)", 
        "Invalid ID",
        fmt::format("Trying to unlock the Lock {} for a system that doesn't exist with ID {}", lock_name, system_label)
    );
    return false;
}

bool SystemRegistrar::HandleSystemEvent(logic::Event& event, std::string system_label) {
    if (HasSystem(system_label)) {
        return system_list_[system_label].lock()->HandleEvent(event);
    }
    global_context_.main_console().IssueWarning(
        "systems::SystemRegistrar::UndoUnlockAndUnload(logic::Event& event, std::size_t)", 
        "Invalid ID",
        fmt::format("Trying to handle system event for system that doesnt exist with ID {} and Event Label {}", system_label, event.event_label)
    );
    return false;
}

bool SystemRegistrar::HandleRegistrarEvent(logic::Event& event) {
    return true;
}

}