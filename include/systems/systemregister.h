#ifndef VVV_SYSTEMREGISTER_H_INCLUDED
#define VVV_SYSTEMREGISTER_H_INCLUDED

#include <memory>
#include <map>

namespace systems {
class AbstractSystem;
class SystemRegistrar;

class SystemRegister {
    protected:

    SystemRegistrar& parent_registrar_;

    std::string register_name_;

    std::map<std::string, std::shared_ptr<AbstractSystem>> core_systems_;

    public:

    virtual ~SystemRegister() {}
    
    SystemRegister() = delete;
    SystemRegister(SystemRegistrar& parent_registrar, std::string register_name);
    
    /**
     * @brief Gets the Scene that is related to the scene_id
     * 
     * @param system_label_id External name for the System
     * @return std::shared_ptr<systems::AbstractSystem>  
     */
    virtual std::weak_ptr<AbstractSystem> GetSystem(std::string system_label_id);

    /**
     * @brief Registers all the systems within the register with the parent registrar
     * 
     */
    void RegisterAllSystems();
};

}
#endif
